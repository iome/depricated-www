<?php
/*************************
  Coppermine Photo Gallery
  ************************
  Copyright (c) 2003-2009 Coppermine Dev Team
  v1.1 originally written by Gregory DEMAR

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.
  
  ********************************************
  Coppermine version: 1.4.21
  $HeadURL: https://coppermine.svn.sourceforge.net/svnroot/coppermine/trunk/cpg1.4.x/themes/classic/theme.php $
  $Revision: 5728 $
  $Author: gaugau $
  $Date: 2009-03-02 09:25:17 +0100 (Mo, 02 Mär 2009) $
**********************************************/
//////////////"themes" was replaced with "$THEME_URL."
// ------------------------------------------------------------------------- //
// This theme has all CORE items removed                                     //
// ------------------------------------------------------------------------- //
define('THEME_IS_XHTML10_TRANSITIONAL',1);

?>