<?php
/*************************
  Coppermine Photo Gallery
  ************************
  Copyright (c) 2003-2008 Dev Team
  v1.1 originally written by Gregory DEMAR

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.
  
  ********************************************
  Coppermine version: 1.4.19
  $HeadURL: https://coppermine.svn.sourceforge.net/svnroot/coppermine/trunk/cpg1.4.x/themes/classic/theme.php $
  $Revision: 4392 $
  $Author: gaugau $
  $Date: 2008-04-16 09:25:35 +0200 (Mi, 16 Apr 2008) $
**********************************************/

//define('THEME_IS_XHTML10_TRANSITIONAL',1);
define('THEME_HAS_FILM_STRIP_GRAPHIC', 1); 

// These parameters overide what the user inputs in the Configuration setup to prevent the theme from breaking.
$CONFIG['max_film_strip_items'] = 5; //overrides the number of thumbnails.
$CONFIG['thumbcols'] = 4; //overrides the number of columns for thumbnails.
$CONFIG['main_table_width'] = '100%'; //overrides the Width of the main table (pixels or %).
$CONFIG['picture_table_width'] = '100%'; //overrides the Width of the table for file display (pixels or %).
$CONFIG['album_list_cols'] = 2; // sets "Number of columns for the album list = 2"
$CONFIG['first_level'] = 1; //sets "Show first level album thumbnails in categories = no".

// HTML template for template sys_menu spacer
$template_sys_menu_spacer =":";

// HTML template for gallery admin menu
if (!isset($template_gallery_admin_menu)) { //{THEMES}
$template_gallery_admin_menu = <<<EOT

<div class="forabg">
  <div class="inner"> <span class="corners-top"><span></span></span>
	<ul class="topiclist">
	  <li class="header">
		<dl class="icon">
          <dt style="width: 100%;">{ADMIN_LNK}</dt>
        </dl>
      </li>
    </ul>
	<ul class="topiclist forums">
	<li class="row bg1">
		<dl class="icon">
          <dd style="width: 100%;">
		 
                <div align="center">
                <table cellpadding="0" cellspacing="1">
                        <tr>
<!-- BEGIN admin_approval -->
                                <td class="admin_menu" id="admin_menu_anim"><a href="editpics.php?mode=upload_approval" title="{UPL_APP_TITLE}">{UPL_APP_LNK}</a></td>
<!-- END admin_approval -->
                                <td class="admin_menu"><a href="admin.php" title="{ADMIN_TITLE}">{ADMIN_LNK}</a></td>
                                <td class="admin_menu"><a href="catmgr.php" title="{CATEGORIES_TITLE}">{CATEGORIES_LNK}</a></td>
                                <td class="admin_menu"><a href="albmgr.php{CATL}" title="{ALBUMS_TITLE}">{ALBUMS_LNK}</a></td>
                                <td class="admin_menu"><a href="groupmgr.php" title="{GROUPS_TITLE}">{GROUPS_LNK}</a></td>
                                <td class="admin_menu"><a href="usermgr.php" title="{USERS_TITLE}">{USERS_LNK}</a></td>
                                <td class="admin_menu"><a href="banning.php" title="{BAN_TITLE}">{BAN_LNK}</a></td>
                                <td class="admin_menu"><a href="reviewcom.php" title="{COMMENTS_TITLE}">{COMMENTS_LNK}</a></td>
<!-- BEGIN log_ecards -->
                                <td class="admin_menu"><a href="db_ecard.php" title="{DB_ECARD_TITLE}">{DB_ECARD_LNK}</a></td>
<!-- END log_ecards -->
                                <td class="admin_menu"><a href="picmgr.php" title="{PICTURES_TITLE}">{PICTURES_LNK}</a></td>
                                <td class="admin_menu"><a href="searchnew.php" title="{SEARCHNEW_TITLE}">{SEARCHNEW_LNK}</a></td>
                                <td class="admin_menu"><a href="util.php" title="{UTIL_TITLE}">{UTIL_LNK}</a></td>
                                <td class="admin_menu"><a href="profile.php?op=edit_profile" title="{MY_PROF_TITLE}">{MY_PROF_LNK}</a></td>
<!-- BEGIN documentation -->
                                <td class="admin_menu"><a href="{DOCUMENTATION_HREF}" title="{DOCUMENTATION_TITLE}" target="cpg_documentation">{DOCUMENTATION_LNK}</a></td>
<!-- END documentation -->
                        </tr>
                </table>
                </div>
		 
		  </dd>
        </dl>
      </li>
    </ul>
    <span class="corners-bottom"><span></span></span> </div>
</div>

EOT;
}  //{THEMES}

// HTML template for user admin menu
if (!isset($template_user_admin_menu)) {  //{THEMES}
$template_user_admin_menu = <<<EOT

<div class="forabg">
  <div class="inner"> <span class="corners-top"><span></span></span>
	<ul class="topiclist">
	  <li class="header">
		<dl class="icon">
          <dt style="width: 100%;">{ALBMGR_LNK}</dt>
        </dl>
      </li>
    </ul>
	<ul class="topiclist forums">
	<li class="row bg1">
		<dl class="icon">
          <dd style="width: 100%;">
		 
                <div align="center">
                <table cellpadding="0" cellspacing="1">
                        <tr>
                                <td class="admin_menu"><a href="albmgr.php" title="{ALBMGR_TITLE}">{ALBMGR_LNK}</a></td>
                                <td class="admin_menu"><a href="modifyalb.php" title="{MODIFYALB_TITLE}">{MODIFYALB_LNK}</a></td>
                                <td class="admin_menu"><a href="profile.php?op=edit_profile" title="{MY_PROF_TITLE}">{MY_PROF_LNK}</a></td>
                                <td class="admin_menu"><a href="picmgr.php" title="{PICTURES_TITLE}">{PICTURES_LNK}</a></td>
                        </tr>
                </table>
                </div>
		 
		  </dd>
        </dl>
      </li>
    </ul>
    <span class="corners-bottom"><span></span></span> </div>
</div>

EOT;
}  //{THEMES}

// HTML template for the category list
if (!isset($template_cat_list)) { //{THEMES}
$template_cat_list = <<<EOT
<!-- BEGIN header -->
         <tr>
                <td colspan="3" width"100%">
			
	<div class="forabg">
		<div class="inner"><span class="corners-top"><span></span></span>
			<ul class="topiclist">
			  <li class="header">
				<dl class="icon">
					<dt>{CATEGORY}</dt>
					<dd class="posts">{ALBUMS}</dd>
					<dd class="views">{PICTURES}</dd>
				</dl>
			  </li>
			</ul>
			<ul class="topiclist forums">
		
<!-- END header -->
<!-- BEGIN catrow_noalb -->
		<li class="row bg2">
			<dl class="icon">
                <table border="0"><tr><td align="left">{CAT_THUMB}</td><td align="left"><span class="catlink"><b>{CAT_TITLE}</b></span>{CAT_DESC}</td></tr></table>
			</dl>
		</li>
<!-- END catrow_noalb -->
<!-- BEGIN catrow -->
		<li class="row bg1">
			<dl class="icon">
				<dt>{CAT_THUMB}<span class="catlink"><b>{CAT_TITLE}</b></span>{CAT_DESC}</dt>
				<dd class="posts"><span>{ALB_COUNT}</span></dd>
				<dd class="views"><span>{PIC_COUNT}</span></dd>
			</dl>
			<dl>
				<dt style="width: 100%;">{CAT_ALBUMS}</dd>
			</dl>
		</li>
<!-- END catrow -->
<!-- BEGIN footer -->
 			</ul>
		<span class="corners-bottom"><span></span></span></div>
	</div>
				
				</td>
        </tr>
       <tr>
                <td colspan="3" align="center">
				
					<div class="panel">
					   <div class="inner"><span class="corners-top"><span></span></span>
					
									<span>{STATISTICS}</span>
							 
					   <span class="corners-bottom"><span></span></span></div>
					</div>
				
				</td>
        </tr>
<!-- END footer -->
<!-- BEGIN spacer -->
        <img src="images/spacer.gif" width="1" height="7" border="" alt="" /><br />
<!-- END spacer -->

EOT;
}  //{THEMES}

// HTML template for the breadcrumb
if (!isset($template_breadcrumb)) { //{THEMES}
$template_breadcrumb = <<<EOT
<!-- BEGIN breadcrumb -->
		<div class="panel bg3" align="left">
			<div class="inner"><span class="corners-top"><span></span></span>

       			<span class="statlink"><b>{BREADCRUMB}</b></span>

			<span class="corners-bottom"><span></span></span></div>
		</div>
<!-- END breadcrumb -->
<!-- BEGIN breadcrumb_user_gal -->
        <tr>
                <td colspan="3" class="tableh1">
				
				<div class="navbar">
					<div class="inner"><span class="corners-top"><span></span></span>
		
					   <table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
								<td align="left"><span class="statlink"><b>{BREADCRUMB}</b></span></td>
								<td align="right"><span class="statlink">{STATISTICS}</span></td>
						</tr>
						</table>
		
					<span class="corners-bottom"><span></span></span></div>
				</div>
		
                </td>
        </tr>
<!-- END breadcrumb_user_gal -->

EOT;
}  //{THEMES}

// HTML template for the album list
if (!isset($template_album_list)) { //{THEMES}
$template_album_list = <<<EOT

<!-- BEGIN stat_row -->
        <tr>
                <td colspan="{COLUMNS}" class="tableh1" align="center"><span class="statlink">{STATISTICS}</span></td>
        </tr>
<!-- END stat_row -->
<!-- BEGIN header -->
        <tr class="tableb_compact">
<!-- END header -->
<!-- BEGIN album_cell -->
        <td width="{COL_WIDTH}%" valign="top">
				
<div class="forabg">
  <div class="inner"><span class="corners-top"><span></span></span>
    <ul class="topiclist">
	  <li class="header">
		<dl class="icon">
          <dt style="width: 100%;"><a href="{ALB_LINK_TGT}"><b>{ALBUM_TITLE}</b></a></dt>
        </dl>
      </li>
    </ul>
	<ul class="topiclist forums">
	<li class="row bg1">
		<dl class="icon">
          <dd style="width: 100%;">
		 
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
					<td colspan="3">
							<img src="images/spacer.gif" width="1" height="1" border="0" alt="" /><br />
					</td>
			</tr>
			<tr>
					<td align="center" valign="middle" class="thumbnails">
							<img src="images/spacer.gif" width="{THUMB_CELL_WIDTH}" height="1" class="image" style="margin-top: 0px; margin-bottom: 0px; border: none;" alt="" /><br />
							<a href="{ALB_LINK_TGT}" class="albums">{ALB_LINK_PIC}<br /></a>
					</td>
					<td>
							<img src="images/spacer.gif" width="1" height="1" border="0" alt="" />
					</td>
					<td width="100%" valign="top" align="left" class="tableb_compact">
							{ADMIN_MENU}
							<p>{ALB_DESC}</p>
							<p class="album_stat">{ALB_INFOS}</p>
					</td>
			</tr>
			</table>
		
		  </dt>
        </dl>
      </li>
    </ul>
    <span class="corners-bottom"><span></span></span></div>
</div>
		
        </td>
<!-- END album_cell -->
<!-- BEGIN empty_cell -->
        <td width="{COL_WIDTH}%" valign="top">
				
		<div class="panel">
			<div class="inner"><span class="corners-top"><span></span></span>
		
        <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
                <td height="1" valign="top">
                        <b>&nbsp;</b>
                </td>
        </tr>
        <tr>
                <td>
                        <img src="images/spacer.gif" width="1" height="1" border="0" alt="" /><br />
                </td>
        </tr>
        <tr>
                <td width="100%" valign="top" class="tableb_compact">
                    <div class="thumbnails" style="background-color:transparent"><img src="images/spacer.gif" width="1" height="1" border="0" class="image" style="border:0;margin-top:1px;margin-bottom:0" alt="" /></div>
                </td>
        </tr>
        </table>
		
			<span class="corners-bottom"><span></span></span></div>
		</div>
		
        </td>
<!-- END empty_cell -->
<!-- BEGIN row_separator -->
        </tr>
        <tr class="tableb_compact">
<!-- END row_separator -->
<!-- BEGIN footer -->
        </tr>
<!-- END footer -->
<!-- BEGIN tabs -->
        <tr>
                <td colspan="{COLUMNS}" style="padding: 0px;">
				
				<div class="navbar">
					<div class="inner"><span class="corners-top"><span></span></span>
		
                        <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                       {TABS}
                                </tr>
                        </table>
		
					<span class="corners-bottom"><span></span></span></div>
				</div>
		
                </td>
        </tr>
<!-- END tabs -->
<!-- BEGIN spacer -->
        <img src="images/spacer.gif" width="1" height="7" border="" alt="" /><br />
<!-- END spacer -->

EOT;
}  //{THEMES}

// HTML template for intermediate image display
if (!isset($template_display_media)) { //{THEMES}
$template_display_media = <<<EOT
                <td align="center" valign="top" height="{CELL_HEIGHT}" style="white-space: nowrap; padding: 0px;">
				
<div class="forabg">
  <div class="inner"> <span class="corners-top"><span></span></span>
	<ul class="topiclist">
	  <li class="header">
		<dl class="icon">
          <dt style="width: 100%;">&nbsp;</dt>
        </dl>
      </li>
    </ul>
	<ul class="topiclist forums">
	<li class="row bg1">
		<dl class="icon">
          <dd style="width: 100%;">
		
                        <table cellspacing="2" cellpadding="0" class="imageborder">
                               <tr>
                                        <td align="center">
                                                {IMAGE}
                                                {ADMIN_MENU}
                                        </td>
                               </tr>
                        </table>
<!-- BEGIN img_desc -->
                        <table cellpadding="0" cellspacing="0" class="tableb">
<!-- BEGIN title -->
                                <tr>
                                        <td class="tableb"><center><b>
                                                {TITLE}
                                        </b></center></td>
                                </tr>
<!-- END title -->
<!-- BEGIN caption -->
                                <tr>
                                        <td class="tableb"><center>
                                                {CAPTION}
                                        </center></td>
                                </tr>
<!-- END caption -->
                        </table>
<!-- END img_desc -->
		
            </dd>
         </dl></li>
      </ul>
      <span class="corners-bottom"><span></span></span>
   </div>
</div>
		
                </td>
EOT;
}  //{THEMES}

// HTML template for filmstrip display
if (!isset($template_film_strip)) { //{THEMES}
$template_film_strip = <<<EOT
<tr><td>
				
		<div class="navbar">
			<div class="inner"><span class="corners-top"><span></span></span>
		
	<table width="100%" cellspacing="0" cellpadding="0">			
		<tr>
		<td valign="top" style="background-image: url({TILE1});"><img src="{TILE1}" alt="" border="0" /></td>
		<td>
         	{THUMB_STRIP}
		</td> 
		<td valign="top" style="background-image: url({TILE2});"><img src="{TILE2}" alt="" border="0" /></td>
        	</tr>
	</table>
		
			<span class="corners-bottom"><span></span></span></div>
		</div>
		
</td></tr>
<!-- BEGIN thumb_cell -->
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" valign="bottom">
				<a href="{LINK_TGT}">{THUMB}</a>
				</td>
			</tr>
			</table>
			{CAPTION}
			{ADMIN_MENU}
<!-- END thumb_cell -->
<!-- BEGIN empty_cell -->
                <td valign="top" align="center">&nbsp;</td>
<!-- END empty_cell -->

EOT;
}  //{THEMES}

// HTML template for the ALBUM admin menu displayed in the album list
if (!isset($template_album_admin_menu)) { //{THEMES}
$template_album_admin_menu = <<<EOT
        <table border="0" cellpadding="0" cellspacing="1">
                <tr>
                        <td align="center" valign="middle" class="admin_menu">
                                <a href="delete.php?id={ALBUM_ID}&amp;what=album"  class="adm_menu" onclick="return confirm('{CONFIRM_DELETE}');">{DELETE}</a>
                        </td>
                        <td align="center" valign="middle" class="admin_menu">
                                <a href="modifyalb.php?album={ALBUM_ID}"  class="adm_menu">{MODIFY}</a>
                        </td>
                        <td align="center" valign="middle" class="admin_menu">
                                <a href="editpics.php?album={ALBUM_ID}"  class="adm_menu">{EDIT_PICS}</a>
                        </td>
                </tr>
        </table>

EOT;
}  //{THEMES}

// HTML template for title row of the thumbnail view (album title + sort options)
if (!isset($template_thumb_view_title_row)) { //{THEMES}
$template_thumb_view_title_row = <<<EOT
				
				<div class="navbar">
					<div class="inner"><span class="corners-top"><span></span></span>

                        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                                <td width="100%" class="statlink"><h2>{ALBUM_NAME}</h2></td>
                                <td><img src="images/spacer.gif" width="1" alt="" /></td>
                                <td>
                                        <table cellpadding="0" cellspacing="0">
                                        <tr>
                                                <td>{TITLE}</td>
                                                <td><span class="statlink"><a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=ta" title="{SORT_TA}">&nbsp;+&nbsp;</a></span></td>
                                                <td><span class="statlink"><a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=td" title="{SORT_TD}">&nbsp;-&nbsp;</a></span></td>
                                        </tr>
                                        <tr>
                                                <td>{NAME}</td>
                                                <td><span class="statlink"><a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=na" title="{SORT_NA}">&nbsp;+&nbsp;</a></span></td>
                                                <td><span class="statlink"><a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=nd" title="{SORT_ND}">&nbsp;-&nbsp;</a></span></td>
                                        </tr>
                                        <tr>
                                                <td>{DATE}</td>
                                                <td><span class="statlink"><a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=da" title="{SORT_DA}">&nbsp;+&nbsp;</a></span></td>
                                                <td><span class="statlink"><a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=dd" title="{SORT_DD}">&nbsp;-&nbsp;</a></span></td>
                                        </tr>
                                        <tr>
                                                <td>{POSITION}</td>
                                                <td><span class="statlink"><a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=pa" title="{SORT_PA}">&nbsp;+&nbsp;</a></span></td>
                                                <td><span class="statlink"><a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=pd" title="{SORT_PD}">&nbsp;-&nbsp;</a></span></td>
                                        </tr>
                                        </table>
                                </td>
                        </tr>
                        </table>
		
		<span class="corners-bottom"><span></span></span></div>
	</div>

EOT;
}  //{THEMES}

// HTML template for title row of the fav thumbnail view (album title + download)
if (!isset($template_fav_thumb_view_title_row)) { //{THEMES}
$template_fav_thumb_view_title_row = <<<EOT

                        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                                <td width="100%" class="statlink"><h2>{ALBUM_NAME}</h2></td>
                                <td><img src="images/spacer.gif" width="1" alt=""></td>
                                <td class="sortorder_cell">
                                        <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                        <td class="sortorder_options"><span class="statlink"><a href="zipdownload.php">{DOWNLOAD_ZIP}</a></span></td>
                                                </tr>
                                                </table>
                                </td>
                        </tr>
                        </table>

EOT;
}  //{THEMES}

// HTML template for thumbnails display
if (!isset($template_thumbnail_view)) { //{THEMES}
$template_thumbnail_view = <<<EOT

<!-- BEGIN header -->
        <tr>
<!-- END header -->
<!-- BEGIN thumb_cell -->
        <td valign="top" class="thumbnails" width ="{CELL_WIDTH}" align="center">
		
	<div class="panel">
	   <div class="inner">
		  <span class="corners-top"><span></span></span>

                <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                                <td align="center">
                                        <a href="{LINK_TGT}">{THUMB}</a>
                                        {CAPTION}
                                        {ADMIN_MENU}
                                </td>
                        </tr>
                </table>
		 
		  <span class="corners-bottom"><span></span></span>
	   </div>
	</div>
				
        </td>
<!-- END thumb_cell -->
<!-- BEGIN empty_cell -->
                <td valign="top" class="thumbnails" align="center">&nbsp;</td>
<!-- END empty_cell -->
<!-- BEGIN row_separator -->
        </tr>
        <tr>
<!-- END row_separator -->
<!-- BEGIN footer -->
        </tr>
<!-- END footer -->
<!-- BEGIN tabs -->
        <tr>
                <td colspan="{THUMB_COLS}" style="padding: 0px;">
				
				<div class="navbar">
					<div class="inner"><span class="corners-top"><span></span></span>
		
                        <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                       {TABS}
                                </tr>
                        </table>
		
					<span class="corners-bottom"><span></span></span></div>
				</div>
		
                </td>
        </tr>
<!-- END tabs -->
<!-- BEGIN spacer -->
        <img src="images/spacer.gif" width="1" height="7" border="" alt="" /><br />
<!-- END spacer -->

EOT;
}  //{THEMES}

// HTML template for the thumbnail view when there is no picture to show
if (!isset($template_no_img_to_display)) { //{THEMES}
$template_no_img_to_display = <<<EOT
        <tr>
                <td class="tableb" height="200" align="center">
                        <font size="3"><b>{TEXT}</b></font>
                </td>
        </tr>
<!-- BEGIN spacer -->
        <img src="images/spacer.gif" width="1" height="7" border="" alt="" /><br />
<!-- END spacer -->

EOT;
}  //{THEMES}

// HTML template for the USER info box in the user list view
if (!isset($template_user_list_info_box))  //{THEMES}
$template_user_list_info_box = <<<EOT

        <table cellspacing="1" cellpadding="0" border="0" width="100%" class="user_thumb_infobox">
                <tr>
                        <th><a href="profile.php?uid={USER_ID}">{USER_NAME}</a></th>
                </tr>
                <tr>
                        <td>{ALBUMS}</td>
                </tr>
                <tr>
                        <td>{PICTURES}</td>
                </tr>
        </table>

EOT;

// HTML template for the image navigation bar
if (!isset($template_img_navbar)) { //{THEMES}
$template_img_navbar = <<<EOT
        <tr>
                <td colspan="{COLUMNS}" style="padding: 0px;">
				
<div class="panel bg2">
	<div class="inner"><span class="corners-top"><span></span></span>
	<div style="width: 100%;">

        <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
                <td align="center" valign="middle" class="navmenu" width="48">
                        <a href="{THUMB_TGT}" class="navmenu_pic" title="{THUMB_TITLE}"><img src="{LOCATION}images/thumbnails.gif" align="middle" border="0" alt="{THUMB_TITLE}" /></a>
                </td>
                <td align="center" valign="middle" class="navmenu" width="48">
                        <a href="javascript:;" class="navmenu_pic" onclick="blocking('picinfo','yes', 'block'); return false;" title="{PIC_INFO_TITLE}"><img src="{LOCATION}images/info.gif" border="0" align="middle" alt="{PIC_INFO_TITLE}" /></a>
                </td>
                <td align="center" valign="middle" class="navmenu" width="48">
                        <a href="{SLIDESHOW_TGT}" class="navmenu_pic" title="{SLIDESHOW_TITLE}"><img src="{LOCATION}images/slideshow.gif" border="0" align="middle" alt="{SLIDESHOW_TITLE}" /></a>
                </td>
                <td align="center" valign="middle" class="navmenu" width="100%">
                        {PIC_POS}
                </td>
<!-- BEGIN report_file_button -->
                <td align="center" valign="middle" class="navmenu" width="48">
                        <a href="{REPORT_TGT}" class="navmenu_pic" title="{REPORT_TITLE}"><img src="{LOCATION}images/report.gif" border="0" align="middle" alt="{REPORT_TITLE}" /></a>
                </td>
<!-- END report_file_button -->
<!-- BEGIN ecard_button -->
                <td align="center" valign="middle" class="navmenu" width="48">
                        <a href="{ECARD_TGT}" class="navmenu_pic" title="{ECARD_TITLE}"><img src="{LOCATION}images/ecard.gif"  border="0" align="middle" alt="{ECARD_TITLE}" /></a>
                </td>
<!-- END ecard_button -->
                <td align="center" valign="middle" class="navmenu" width="48">
                        <a href="{PREV_TGT}" class="navmenu_pic" title="{PREV_TITLE}"><img src="{LOCATION}images/prev.gif"  border="0" align="middle" alt="{PREV_TITLE}" /></a>
                </td>
                <td align="center" valign="middle" class="navmenu" width="48">
                        <a href="{NEXT_TGT}" class="navmenu_pic" title="{NEXT_TITLE}"><img src="{LOCATION}images/next.gif"  border="0" align="middle" alt="{NEXT_TITLE}" /></a>
                </td>
        </tr>
        </table>
		 
	</div>
	<span class="corners-bottom"><span></span></span></div>
</div>
                </td>
        </tr>
EOT;
}  //{THEMES}

// HTML template for the image rating box
if (!isset($template_image_rating)) { //{THEMES}
$template_image_rating = <<<EOT
        <tr>
                <td colspan="{COLUMNS}" style="padding: 0px;">
				
<div class="panel bg2">
	<div class="inner"><span class="corners-top"><span></span></span>
	<div style="width: 100%;">

<table align="center" width="{WIDTH}" cellspacing="1" cellpadding="0" class="maintable">
        <tr>
                <td colspan="6" class="tableh2_compact"><b>{TITLE}</b> {VOTES}</td>
        </tr>
        <tr>
                <td class="tableb_compact" width="17%" align="center"><a href="{RATE0}" title="{RUBBISH}" rel="nofollow"><img src="{LOCATION}images/rating0.gif" border="0" alt="{RUBBISH}" /><br /></a></td>
                <td class="tableb_compact" width="17%" align="center"><a href="{RATE1}" title="{POOR}" rel="nofollow"><img src="{LOCATION}images/rating1.gif" border="0" alt="{POOR}" /><br /></a></td>
                <td class="tableb_compact" width="17%" align="center"><a href="{RATE2}" title="{FAIR}" rel="nofollow"><img src="{LOCATION}images/rating2.gif" border="0" alt="{FAIR}" /><br /></a></td>
                <td class="tableb_compact" width="17%" align="center"><a href="{RATE3}" title="{GOOD}" rel="nofollow"><img src="{LOCATION}images/rating3.gif" border="0" alt="{GOOD}" /><br /></a></td>
                <td class="tableb_compact" width="17%" align="center"><a href="{RATE4}" title="{EXCELLENT}" rel="nofollow"><img src="{LOCATION}images/rating4.gif" border="0" alt="{EXCELLENT}" /><br /></a></td>
                <td class="tableb_compact" width="17%" align="center"><a href="{RATE5}" title="{GREAT}" rel="nofollow"><img src="{LOCATION}images/rating5.gif" border="0" alt="{GREAT}" /><br /></a></td>
        </tr>
</table>
		 
	</div>
	<span class="corners-bottom"><span></span></span></div>
</div>
                </td>
        </tr>
EOT;
}  //{THEMES}

// HTML template for the display of comments
if (!isset($template_image_comments)) { //{THEMES}
$template_image_comments = <<<EOT
        <tr>
                <td colspan="{COLUMNS}" style="padding: 0px;">
				
<div class="panel bg1">
	<div class="inner"><span class="corners-top"><span></span></span>
	<div style="width: 100%;">

<table align="center" width="{WIDTH}" cellspacing="1" cellpadding="0" class="maintable">

        <tr>
                <td>
                        <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                                <td class="tableh2_compact" nowrap="nowrap">
                                        <b>{MSG_AUTHOR}</b><a name="comment{MSG_ID}"></a>&nbsp;
<!-- BEGIN ipinfo -->
                                                                                 ({IP})
<!-- END ipinfo -->
</td>


                                <td class="tableh2_compact" align="right" width="100%">
<!-- BEGIN report_comment_button -->
     <a href="report_file.php?pid={PID}&amp;msg_id={MSG_ID}&amp;what=comment" title="{REPORT_COMMENT_TITLE}"><img src="images/report.gif" width="16" height="16" border="0" align="middle" alt="{REPORT_COMMENT_TITLE}" /></a>
<!-- END report_comment_button -->


<!-- BEGIN buttons -->
                                        <a href="javascript:;" onclick="blocking('cbody{MSG_ID}','', 'block'); blocking('cedit{MSG_ID}','', 'block'); return false;" title="{EDIT_TITLE}"><img src="images/edit.gif" border="0" align="middle" /></a>
                                        <a href="delete.php?msg_id={MSG_ID}&what=comment"  onclick="return confirm('{CONFIRM_DELETE}');"><img src="images/delete.gif" border="0" align="middle" /></a>
<!-- END buttons -->
                                </td>
                                <td class="tableh2_compact" align="right" nowrap="nowrap">
                                        <span class="comment_date">[{MSG_DATE}]</span>
                                </td></tr>
                        </table>
                </td>
        </tr>
        <tr>
                <td class="tableb_compact">
                        <div id="cbody{MSG_ID}" style="display:block">
                                {MSG_BODY}
                        </div>
                        <div id="cedit{MSG_ID}" style="display:none">
<!-- BEGIN edit_box_smilies -->
                                <table width="100%" cellpadding="0" cellspacing="0">

                                                <form name="f{MSG_ID}" method="POST" action="db_input.php">
                                                <input type="hidden" name="event" value="comment_update" />
                                                <input type="hidden" name="msg_id" value="{MSG_ID}" />
                                                <tr>
                                                <td>
                                                   <input type="text" name="msg_author" value="{MSG_AUTHOR}" class="textinput" size="25" />
                                                </td><td>
                                                </tr>
                                                <tr>
                                                <td width="80%">
                                                        <textarea cols="40" rows="2" class="textinput" name="msg_body" onselect="storeCaret_f{MSG_ID}(this);" onclick="storeCaret_f{MSG_ID}(this);" onkeyup="storeCaret_f{MSG_ID}(this);" style="width: 100%;">{MSG_BODY_RAW}</textarea>
                                                </td>
                                                <td class="tableb_compact">
                                                </td>
                                                <td>
                                                        <input type="submit" class="comment_button" name="submit" value="{OK}" />
                                                </td>
                                                </form>
                                        </tr>
                                        <tr>
                                                <td colspan="3"><img src="images/spacer.gif" width="1" height="2" /><br /></td>
                                        </tr>
                                </table>
                                {SMILIES}
<!-- END edit_box_smilies -->
<!-- BEGIN edit_box_no_smilies -->
                                <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                                <form name="f{MSG_ID}" method="POST" action="db_input.php">
                                                <input type="hidden" name="event" value="comment_update" />
                                                <input type="hidden" name="msg_id" value="{MSG_ID}" />
                                                <td>
                                                <input type="text" name="msg_author" value="{MSG_AUTHOR}" class="textinput" size="25" />
                                                </td>
                                        </tr>
                                        <tr>
                                                <td width="100%">
                                                        <textarea cols="40" rows="2" class="textinput" name="msg_body" style="width: 100%;">{MSG_BODY_RAW}</textarea>
                                                </td>
                                                <td class="tableb_compact">
                                                </td>
                                                <td>
                                                        <input type="submit" class="comment_button" name="submit" value="{OK}" />
                                                </td>
                                                </form>
                                        </tr>
                                        <tr>
                                                <td colspan="3"><img src="images/spacer.gif" width="1" height="2" /><br /></td>
                                        </tr>
                                </table>
<!-- END edit_box_no_smilies -->
                        </div>
                </td>
        </tr>
</table>
		 
	</div>
	<span class="corners-bottom"><span></span></span></div>
</div>
                </td>
        </tr>
EOT;
}  //{THEMES}

// HTML template for the form to add comments
if (!isset($template_add_your_comment)) { //{THEMES}
$template_add_your_comment = <<<EOT
        <form method="post" name="post" action="db_input.php">
         <tr>
                <td colspan="{COLUMNS}" style="padding: 0px;">
				
<div class="panel bg2">
	<div class="inner"><span class="corners-top"><span></span></span>
	<div style="width: 100%;">

               <table align="center" width="{WIDTH}" cellspacing="1" cellpadding="0" class="maintable">
                        <tr>
                                        <td width="100%" class="tableh2_compact"><b>{ADD_YOUR_COMMENT}</b></td>
                        </tr>
                        <tr>
                <td colspan="1">
                        <table width="100%" cellpadding="0" cellspacing="0">

<!-- BEGIN user_name_input -->
                                                        <tr>
                                                                <td class="tableb_compact">
                                        {NAME}
                                </td>
                                <td class="tableb_compact">
                                        <input type="text" class="textinput" name="msg_author" size="10" maxlength="20" value="{USER_NAME}" />
                                </td>
<!-- END user_name_input -->
<!-- BEGIN input_box_smilies -->
                                <td class="tableb_compact">
                                {COMMENT}
                                                                </td>
                                <td width="100%" class="tableb_compact">
                                <input type="text" class="textinput" id="message" name="msg_body" onselect="storeCaret_post(this);" onclick="storeCaret_post(this);" onkeyup="storeCaret_post(this);" maxlength="{MAX_COM_LENGTH}" style="width: 100%;" />
                                                                </td>
<!-- END input_box_smilies -->
<!-- BEGIN input_box_no_smilies -->
                                <td class="tableb_compact">
                                {COMMENT}
                                                                </td>
                                <td width="100%" class="tableb_compact">
                                <input type="text" class="textinput" id="message" name="msg_body"  maxlength="{MAX_COM_LENGTH}" style="width: 100%;" />
                                </td>
<!-- END input_box_no_smilies -->
                                <td class="tableb_compact">
                                <input type="hidden" name="event" value="comment" />
                                <input type="hidden" name="pid" value="{PIC_ID}" />
                                <input type="submit" class="comment_button" name="submit" value="{OK}" />
                                </td>
                                                        </tr>
                        </table>
                </td>
        </tr>
<!-- BEGIN smilies -->
        <tr>
                <td width="100%" class="tableb_compact">
                        {SMILIES}
                </td>
        </tr>
<!-- END smilies -->
                </table>
		 
	</div>
	<span class="corners-bottom"><span></span></span></div>
</div>
                </td>
        </tr>
        </form>
EOT;
}  //{THEMES}

// HTML template used by the cpg_die function
if (!isset($template_cpg_die)) { //{THEMES}
$template_cpg_die = <<<EOT

        <tr>
                <td class="tableb" align="center">
                        <font size="3"><b>{MESSAGE}</b></font>
<!-- BEGIN file_line -->
                        <br />
                        <br />
                        {FILE_TXT}{FILE} - {LINE_TXT}{LINE}
<!-- END file_line -->
<!-- BEGIN output_buffer -->
                        <br />
                        <br />
                        <div align="left">
                                {OUTPUT_BUFFER}
                        </div>
<!-- END output_buffer -->
                        <br /><br />
                </td>
        </tr>

EOT;
}  //{THEMES}

// HTML template used by the msg_box function
if (!isset($template_msg_box)) { //{THEMES}
$template_msg_box = <<<EOT

        <tr>
                <td class="tableb" align="center">
                        <font size="3"><b>{MESSAGE}</b></font>
                </td>
        </tr>
<!-- BEGIN button -->
                <tr>
                        <td align="center" class="tablef">
                                <table cellpadding="0" cellspacing="0">
                                        <tr>
                                                <td class="admin_menu">
                                                        <a href="{LINK}">{TEXT}</a>
                                                </td>
                                        </tr>
                                </table>
                        </td>
                </tr>
<!-- END button -->

EOT;
}  //{THEMES}

// Added to display film_strip
function theme_display_film_strip(&$thumb_list, $nbThumb, $album_name, $aid, $cat, $pos, $sort_options, $mode = 'thumb')
{
    global $CONFIG, $THEME_DIR;
    global $template_film_strip, $lang_film_strip;

    static $template = '';
    static $thumb_cell = '';
    static $empty_cell = '';
    static $spacer = '';

    if ((!$template)) {
        $template = $template_film_strip;
        $thumb_cell = template_extract_block($template, 'thumb_cell');
        $empty_cell = template_extract_block($template, 'empty_cell');
    }

    $cat_link = is_numeric($aid) ? '' : '&cat=' . $cat;

    $thumbcols = $CONFIG['thumbcols'];
    $cell_width = ceil(100 / $CONFIG['max_film_strip_items']) . '%';

    $i = 0;
    $thumb_strip = '';
    foreach($thumb_list as $thumb) {
        $i++;
        if ($mode == 'thumb') {
            $params = array('{CELL_WIDTH}' => $cell_width,
                '{LINK_TGT}' => "displayimage.php?album=$aid$cat_link&pos={$thumb['pos']}",
                '{THUMB}' => $thumb['image'],
                '{CAPTION}' => '',
                '{ADMIN_MENU}' => ''
                );
        } else {
            $params = array('{CELL_WIDTH}' => $cell_width,
                '{LINK_TGT}' => "index.php?cat={$thumb['cat']}",
                '{THUMB}' => $thumb['image'],
                '{CAPTION}' => '',
                '{ADMIN_MENU}' => ''
                );
        }
        $thumb_strip .= template_eval($thumb_cell, $params);
    }

    if (defined('THEME_HAS_FILM_STRIP_GRAPHICS')) {
        $tile1 = $THEME_DIR . 'images/tile1.gif';
        $tile2 = $THEME_DIR . 'images/tile2.gif';
    } elseif (defined('THEME_HAS_FILM_STRIP_GRAPHIC')) {
        $tile1=$tile2=$THEME_DIR . 'images/tile.gif';
    } else {
        $tile1=$tile2= 'images/tile.gif';
    }

    $params = array('{THUMB_STRIP}' => $thumb_strip,
        '{COLS}' => $i,
        '{TILE1}' => $tile1,
        '{TILE2}' => $tile2,
        );

    ob_start();
    starttable('');
      echo template_eval($template, $params);
    endtable();
    $film_strip = ob_get_contents();
    ob_end_clean();

    return $film_strip;
}

if (!function_exists('theme_display_image')) {  //{THEMES}
function theme_display_image($nav_menu, $picture, $votes, $pic_info, $comments, $film_strip)
{
    global $HTTP_COOKIE_VARS, $CONFIG;

    starttable();
	    echo $nav_menu;
    endtable();
    starttable();
    if ($CONFIG['display_film_strip'] == 1) {
		echo "<tr><td width='200' class='tableb' valign='middle' ><!-- gb before film_strip -->";
	        echo $film_strip;
		echo "</td><!-- gb after film_strip -->";
    }
		echo "<!-- gb before picture -->";		
    echo $picture;
    echo "</tr><!-- gb after picture -->";
    endtable();

    $picinfo = isset($HTTP_COOKIE_VARS['picinfo']) ? $HTTP_COOKIE_VARS['picinfo'] : ($CONFIG['display_pic_info'] ? 'block' : 'none');
    echo "<div class=\"panel bg3\"><div class=\"inner\"><span class=\"corners-top\"><span></span></span><div id=\"picinfo\" style=\"display: $picinfo;\">\n";
    starttable();
    echo $pic_info;
    endtable();
    echo "</div><span class=\"corners-bottom\"><span></span></span></div></div>\n";

    starttable();
    	echo $votes;
    endtable();

    starttable();
    echo $comments;
    endtable();
}
}  //{THEMES}


?>
