<?php
define('THEME_HAS_NAVBAR_GRAPHICS', 1);
define('THEME_HAS_FILM_STRIP_GRAPHIC',1);
define('THEME_HAS_RATING_GRAPHICS',1);
define('THEME_HAS_SUB_MENU_ICONS',16);

/******************************************************************************
** Section <<<assemble_template_buttons>>> - START
******************************************************************************/
// Creates buttons from a template using an array of tokens
// this function is used in this file it needs to be declared before being called.
function assemble_template_buttons($template_buttons,$buttons) {
    $counter=0;
    $output='';

    foreach ($buttons as $button)  {
      if (isset($button[4])) {
         $spacer=$button[4];
      } else {
      $spacer='';
      }

		if (defined('THEME_HAS_NO_SUBMENU_TEXT')) {
	    	$params = array(
	            '{SPACER}'     => $spacer,
	            '{BLOCK_ID}'   => $button[3],
	            '{HREF_TGT}'   => $button[2],
	            '{HREF_TITLE}' => $button[1],
	            '{HREF_LNK}'   => $spacer,
	            '{HREF_ICON}'   => $button[6],
	            '{HREF_ATTRIBUTES}'   => $button[5]
            );
		} else {
	    	$params = array(
	            '{SPACER}'     => $spacer,
	            '{BLOCK_ID}'   => $button[3],
	            '{HREF_TGT}'   => $button[2],
	            '{HREF_TITLE}' => $button[1],
	            '{HREF_LNK}'   => $button[0],
	            '{HREF_ICON}'   => $button[6],
	            '{HREF_ATTRIBUTES}'   => $button[5]
            );
		}
        $output.=template_eval($template_buttons, $params);
    }
    return $output;
}
/******************************************************************************
** Section <<<assemble_template_buttons>>> - END
******************************************************************************/

/******************************************************************************
** Section <<<addbutton>>> - START
******************************************************************************/
// Creates an array of tokens to be used with function assemble_template_buttons
// this function is used in this file it needs to be declared before being called.
function addbutton(&$menu,$href_lnk,$href_title,$href_tgt,$block_id,$spacer,$href_attrib='',$href_icon) {
  $menu[]=array($href_lnk,$href_title,$href_tgt,$block_id,$spacer,$href_attrib,$href_icon);
}
/******************************************************************************
** Section <<<addbutton>>> - END
******************************************************************************/

/******************************************************************************
** Section <<<$template_sys_menu>>> - START
******************************************************************************/
// HTML template for main menu
$template_sys_menu = <<<EOT

	<ul class="menu">
		<li class="top"><a id="top_level_user" title="{SYS_USER_TITLE}" class="top_link"><b class="b_top"></b><span class="span_top drop">{SYS_USER_LNK}</span><!--[if IE 7]><!--></a><!--<![endif]-->
			<!--[if lte IE 6]><table><tr><td><![endif]-->
			<ul class="sub">
			<li><!-- BEGIN faq --><!-- END faq --></li>
		         {BUTTONS}
	      	</ul>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->
		</li>
	</ul>
	
EOT;
/******************************************************************************
** Section <<<$template_sys_menu>>> - END
******************************************************************************/
// HTML template for sub_menu
$template_sub_menu = <<<EOT

	<ul class="menu">
		<li class="top"><a id="top_level_albums" title="{SUB_ALBUM_TITLE}" class="top_link"><b class="b_top"></b><span class="span_top drop">{SUB_ALBUM_LNK}</span><!--[if IE 7]><!--></a><!--<![endif]-->
			<!--[if lte IE 6]><table><tr><td><![endif]-->
			<ul class="sub">
		         {BUTTONS}
	      	</ul>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->
		</li>
	</ul>

EOT;
/******************************************************************************
** Section <<<$template_sub_menu>>> - START
******************************************************************************/

if (!defined('THEME_HAS_NO_SYS_MENU_BUTTONS')) {

  // HTML template for template sys_menu spacer

  $template_sys_menu_spacer ="";

  // HTML template for template sys_menu buttons
  $template_sys_menu_button = <<<EOT
  <!-- BEGIN {BLOCK_ID} -->
	<li><a href="{HREF_TGT}" title="{HREF_TITLE}" {HREF_ATTRIBUTES}><b class="{HREF_ICON}"></b><span>{HREF_LNK}</span></a></li>{SPACER}
  <!-- END {BLOCK_ID} -->	
EOT;

	// HTML template for template sys_menu buttons
    // {HREF_LNK}{HREF_TITLE}{HREF_TGT}{BLOCK_ID}{SPACER}{HREF_ATTRIBUTES}{HREF_ICON}
    addbutton($sys_menu_buttons,'{HOME_LNK}','{HOME_TITLE}','{HOME_TGT}','home',$template_sys_menu_spacer,'',"Home");
//    addbutton($sys_menu_buttons,'{CONTACT_LNK}','{CONTACT_TITLE}','{CONTACT_TGT}','contact',$template_sys_menu_spacer,'',"Contact");
    addbutton($sys_menu_buttons,'{MY_GAL_LNK}','{MY_GAL_TITLE}','{MY_GAL_TGT}','my_gallery',$template_sys_menu_spacer,'',"My_Gallery");
    addbutton($sys_menu_buttons,'{MEMBERLIST_LNK}','{MEMBERLIST_TITLE}','{MEMBERLIST_TGT}','allow_memberlist',$template_sys_menu_spacer,'',"MemberList");
    if (array_key_exists('allowed_albums', $USER_DATA) && is_array($USER_DATA['allowed_albums']) && count($USER_DATA['allowed_albums'])) {
      addbutton($sys_menu_buttons,'{UPL_APP_LNK}','{UPL_APP_TITLE}','{UPL_APP_TGT}','upload_approval',$template_sys_menu_spacer,'',"Upload_App");
    }
    addbutton($sys_menu_buttons,'{MY_PROF_LNK}','{MY_PROF_TITLE}','{MY_PROF_TGT}','my_profile',$template_sys_menu_spacer,'',"My_Profile");
    addbutton($sys_menu_buttons,'{ADM_MODE_LNK}','{ADM_MODE_TITLE}','{ADM_MODE_TGT}','enter_admin_mode',$template_sys_menu_spacer,'',"Admin_Mode");
    addbutton($sys_menu_buttons,'{USR_MODE_LNK}','{USR_MODE_TITLE}','{USR_MODE_TGT}','leave_admin_mode',$template_sys_menu_spacer,'',"User_Mode");
//    addbutton($sys_menu_buttons,'{SIDEBAR_LNK}','{SIDEBAR_TITLE}','{SIDEBAR_TGT}','sidebar',$template_sys_menu_spacer,'',"SideBar");
    addbutton($sys_menu_buttons,'{UPL_PIC_LNK}','{UPL_PIC_TITLE}','{UPL_PIC_TGT}','upload_pic',$template_sys_menu_spacer,'',"Upload_Pic");
    addbutton($sys_menu_buttons,'{REGISTER_LNK}','{REGISTER_TITLE}','{REGISTER_TGT}','register',$template_sys_menu_spacer,'',"Register");
    addbutton($sys_menu_buttons,'{LOGIN_LNK}','{LOGIN_TITLE}','{LOGIN_TGT}','login','','',"LogIn");
    addbutton($sys_menu_buttons,'{LOGOUT_LNK}','{LOGOUT_TITLE}','{LOGOUT_TGT}','logout','','',"LogOut");
    // Login and Logout don't have a spacer as only one is shown, and either would be the last option.

  $sys_menu_buttons = CPGPluginAPI::filter('sys_menu',$sys_menu_buttons);
  $params = array('{BUTTONS}' => assemble_template_buttons($template_sys_menu_button,$sys_menu_buttons));
  $template_sys_menu = template_eval($template_sys_menu,$params);
}
/******************************************************************************
** Section <<<$template_sub_menu>>> - END
******************************************************************************/

/******************************************************************************
** Section <<<THEME_HAS_NO_SUB_MENU_BUTTONS>>> - START
******************************************************************************/
if (!defined('THEME_HAS_NO_SUB_MENU_BUTTONS')) {

  // HTML template for template sub_menu spacer

  $template_sub_menu_spacer = $template_sys_menu_spacer;

  // HTML template for template sub_menu buttons

  $template_sub_menu_button = <<<EOT
  <!-- BEGIN {BLOCK_ID} -->
	<li><a href="{HREF_TGT}" title="{HREF_TITLE}" {HREF_ATTRIBUTES}><b class="{HREF_ICON}"></b><span>{HREF_LNK}</span></a></li>{SPACER}
  <!-- END {BLOCK_ID} -->	
EOT;

  // HTML template for template sub_menu buttons

    // {HREF_LNK}{HREF_TITLE}{HREF_TGT}{BLOCK_ID}{SPACER}{HREF_ATTRIBUTES}{HREF_CLASS}
    addbutton($sub_menu_buttons,'{CUSTOM_LNK_LNK}','{CUSTOM_LNK_TITLE}','{CUSTOM_LNK_TGT}','custom_link',$template_sub_menu_spacer,'',"Custom_Lnk");
    addbutton($sub_menu_buttons,'{ALB_LIST_LNK}','{ALB_LIST_TITLE}','{ALB_LIST_TGT}','album_list',$template_sub_menu_spacer,'',"Alb_List");
    addbutton($sub_menu_buttons,'{LASTUP_LNK}','{LASTUP_TITLE}','{LASTUP_TGT}','lastup',$template_sub_menu_spacer,'rel="nofollow"',"LastUpload");
    addbutton($sub_menu_buttons,'{LASTCOM_LNK}','{LASTCOM_TITLE}','{LASTCOM_TGT}','lastcom',$template_sub_menu_spacer,'rel="nofollow"',"LastComment");
    addbutton($sub_menu_buttons,'{TOPN_LNK}','{TOPN_TITLE}','{TOPN_TGT}','topn',$template_sub_menu_spacer,'rel="nofollow"',"TopN_Lnk");
    addbutton($sub_menu_buttons,'{TOPRATED_LNK}','{TOPRATED_TITLE}','{TOPRATED_TGT}','toprated',$template_sub_menu_spacer,'rel="nofollow"',"TopRated");
    addbutton($sub_menu_buttons,'{FAV_LNK}','{FAV_TITLE}','{FAV_TGT}','favpics',$template_sub_menu_spacer,'rel="nofollow"',"Favorite");
    if ($CONFIG['browse_by_date'] != 0) {
    	addbutton($sub_menu_buttons, '{BROWSEBYDATE_LNK}', '{BROWSEBYDATE_TITLE}', '{BROWSEBYDATE_TGT}', 'browse_by_date', $template_sub_menu_spacer, 'rel="nofollow" class="greybox"',"Browse");
    }
    addbutton($sub_menu_buttons,'{SEARCH_LNK}','{SEARCH_TITLE}','{SEARCH_TGT}','search',$template_sub_menu_spacer,'',"Search");

  $sub_menu_buttons = CPGPluginAPI::filter('sub_menu',$sub_menu_buttons);
  $params = array('{BUTTONS}' => assemble_template_buttons($template_sub_menu_button,$sub_menu_buttons));
  $template_sub_menu = template_eval($template_sub_menu,$params);
}
/******************************************************************************
** Section <<<THEME_HAS_NO_SUB_MENU_BUTTONS>>> - END
******************************************************************************/

$template_gallery_admin_menu = <<<EOT
<ul class="menu">
<!-- First button without submenus -->	
	<li class="top"><a id="config" title="{ADM_CONFIG_TITLE}" class="top_link"><b class="b_top"></b><span class="span_top drop">{ADM_CONFIG_LNK}</span><!--[if IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->
		<ul class="sub">
			<!-- BEGIN config -->
			<li><a href="admin.php" title="{ADMIN_TITLE}"><b class="config"></b><span>{ADMIN_LNK}</span></a></li>
			<!-- END config -->
			<!-- BEGIN plugin_manager -->
			<li><a href="pluginmgr.php" title="{PLUGINMGR_TITLE}"><b class="plugin"></b><span>{PLUGINMGR_LNK}</span></a></li>
            <!-- END plugin_manager -->
            <!-- BEGIN bridge_manager -->
			<li><a href="bridgemgr.php" title="{BRIDGEMGR_TITLE}"><b class="bridge"></b><span>{BRIDGEMGR_LNK}</span></a></li>
			<!-- END bridge_manager -->
			<!-- BEGIN export -->
			<li><a href="export.php" title="{EXPORT_TITLE}"><b class="export"></b><span>{EXPORT_LNK}</span></a></li>
			<!-- END export -->
			<!-- BEGIN update_database -->
			<li><a href="update.php" title="{UPDATE_DATABASE_TITLE}"><b class="update"></b><span>{UPDATE_DATABASE_LNK}</span></a></li>
			<!-- END update_database -->
			<!-- BEGIN check_versions -->
			<li><a href="versioncheck.php" title="{CHECK_VERSIONS_TITLE}"><b class="check"></b><span>{CHECK_VERSIONS_LNK}</span></a></li>
			<!-- END check_versions -->
			<!-- BEGIN view_log_files -->
			<li><a href="viewlog.php" title="{VIEW_LOG_FILES_TITLE}"><b class="log"></b><span>{VIEW_LOG_FILES_LNK}</span></a></li>
			<!-- END view_log_files -->
			<!-- BEGIN php_info -->
			<li><a href="phpinfo.php" title="{PHPINFO_TITLE}"><b class="php"></b><span>{PHPINFO_LNK}</span></a></li>
			<!-- END php_info -->
			<!-- BEGIN show_news -->
<!--			<li><a href="mode.php?what=news&amp;referer=$REFERER" title="{SHOWNEWS_TITLE}"><b class="news"></b><span>{SHOWNEWS_LNK}</span></a></li> -->
			<!-- END show_news -->
			<!-- BEGIN documentation -->
			<li><a href="{DOCUMENTATION_HREF}" title="{DOCUMENTATION_TITLE}"><b class="docs"></b><span>{DOCUMENTATION_LNK}</span></a></li>
			<!-- END documentation -->
			</ul>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->
	</li>
<!-- Second button without submenus -->	
	<li class="top"><a id="albums" title="{ADM_GALLERY_TITLE}" class="top_link"><b class="b_top"></b><span class="span_top drop">{ADM_GALLERY_LNK}</span><!--[if IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->
		<ul class="sub">
			<!-- BEGIN catmgr -->
			<li><a href="catmgr.php" title="{CATEGORIES_TITLE}"><b class="catmgr"></b><span>{CATEGORIES_LNK}</span></a></li>
			<!-- END catmgr -->
			<!-- BEGIN albmgr -->
			<li><a href="albmgr.php{CATL}" title="{ALBUMS_TITLE}"><b class="albmgr"></b><span>{ALBUMS_LNK}</span></a></li>
			<!-- END albmgr -->
			<!-- BEGIN picmgr -->
			<li><a href="picmgr.php" title="{PICTURES_TITLE}"><b class="picmgr"></b><span>{PICTURES_LNK}</span></a></li>
			<!-- end picmgr -->
			<!-- BEGIN review_comments -->
			<li><a href="reviewcom.php" title="{COMMENTS_TITLE}"><b class="comments"></b><span>{COMMENTS_LNK}</span></a></li>
			<!-- END review_comments -->
			<!-- BEGIN log_ecards -->
			<li><a href="db_ecard.php" title="{DB_ECARD_TITLE}"><b class="ecards"></b><span>{DB_ECARD_LNK}</span></a></li>
			<!-- END log_ecards -->
		</ul>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->
	</li>
<!-- Second button without submenus -->	
	<li class="top"><a id="users" title="{ADM_GROUP_TITLE}" class="top_link"><b class="b_top"></b><span class="span_top drop">{ADM_GROUP_LNK}</span><!--[if IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->
		<ul class="sub">

			<!-- BEGIN admin_approval -->
			<li><a href="editpics.php?mode=upload_approval" title="{UPL_APP_TITLE}"><b class="people"></b><span>{UPL_APP_LNK}</span></a></li>
			<!-- END admin_approval -->
			<!-- BEGIN usermgr -->
			<li><a href="usermgr.php" title="{USERS_TITLE}"><b class="usermgr"></b><span>{USERS_LNK}</span></a></li>
			<!-- END usermgr -->
			<!-- BEGIN groupmgr -->
			<li><a href="groupmgr.php" title="{GROUPS_TITLE}"><b class="groupmgr"></b><span>{GROUPS_LNK}</span></a></li>
			<!-- END groupmgr -->
			<!-- BEGIN banmgr -->
			<li><a href="banning.php" title="{BAN_TITLE}"><b class="banmgr"></b><span>{BAN_LNK}</span></a></li>
			<!-- END banmgr -->
			<!-- BEGIN admin_profile -->
			<li><a href="profile.php?op=edit_profile" title="{MY_PROF_TITLE}"><b class="profile"></b><span>{MY_PROF_LNK}</span></a></li>
			<!-- END admin_profile -->
		</ul>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->
	</li>
<!-- Second button without submenus -->	
	<li class="top"><a id="admin_tools" title="{ADM_MEDIA_TITLE}" class="top_link"><b class="b_top"></b><span class="span_top drop">{ADM_MEDIA_LNK}</span><!--[if IE 7]><!--></a><!--<![endif]-->
		<!--[if lte IE 6]><table><tr><td><![endif]-->
		<ul class="sub">
			<!-- BEGIN admin_tools -->
			<li><a href="util.php?t={TIME_STAMP}#admin_tools" title="{UTIL_TITLE}"><b class="tools"></b><span>{UTIL_LNK}</span></a></li>
			<!-- END admin_tools -->
			<!-- BEGIN batch_add -->
			<li><a href="searchnew.php" title="{SEARCHNEW_TITLE}"><b class="batch"></b><span>{SEARCHNEW_LNK}</span></a></li>
			<!-- END batch_add -->
			<!-- BEGIN exif_manager -->
			<li><a href="exifmgr.php" title="{EXIFMGR_TITLE}"><b class="exif"></b><span>{EXIFMGR_LNK}</span></a></li>
			<!-- END exif_manager -->
			<!-- BEGIN keyword_manager -->
			<li><a href="keywordmgr.php" title="{KEYWORDMGR_TITLE}"><b class="keyword"></b><span>{KEYWORDMGR_LNK}</span></a></li>
			<!-- END keyword_manager -->
			<!-- BEGIN overall_stats -->
			<li><a href="stat_details.php?type=hits&amp;sort=sdate&amp;dir=&amp;sdate=1&amp;ip=1&amp;search_phrase=0&amp;referer=0&amp;browser=1&amp;os=1&amp;mode=fullscreen&amp;page=1&amp;amount=50" title="{OVERALL_STATS_TITLE}"><b class="stats"></b><span>{OVERALL_STATS_LNK}</span></a></li>
			<!-- END overall_stats -->
			</ul>
		<!--[if lte IE 6]></td></tr></table></a><![endif]-->
	</li>
</ul>

EOT;

/******************************************************************************
** Section <<<$template_user_admin_menu>>> - START
******************************************************************************/
// HTML template for user admin menu
$template_user_admin_menu = <<<EOT
<ul class="menu">
	<li class="top"><a id="admin_tools" title="{USER_TOOLS_TITLE}" class="top_link"><b class="b_top"></b><span class="span_top drop">{USER_TOOLS_LNK}</span><!--[if IE 7]><!--></a><!--<![endif]-->
	<!--[if lte IE 6]><table><tr><td><![endif]-->
		<ul class="sub">
		  <li><a href="albmgr.php" 					title="{ALBMGR_TITLE}">		<b class="albmgr"></b>		<span>{ALBMGR_LNK}   </span></a></li>
          <li><a href="modifyalb.php" 				title="{MODIFYALB_TITLE}">	<b class="AlbumProp"></b>	<span>{MODIFYALB_LNK}</span></a></li>
          <li><a href="profile.php?op=edit_profile" title="{MY_PROF_TITLE}">	<b class="profile"></b>		<span>{MY_PROF_LNK}  </span></a></li>
          <li><a href="picmgr.php" 					title="{PICTURES_TITLE}">	<b class="picmgr"></b>	    <span>{PICTURES_LNK} </span></a></li>
        </ul>
	<!--[if lte IE 6]></td></tr></table></a><![endif]-->
	</li>
</ul>
EOT;

/******************************************************************************
** Section <<<$template_thumb_view_title_row>>> - START
******************************************************************************/
$template_thumb_view_title_row = <<<EOT

<table width="100%" cellpadding="0" cellspacing="0">
   <tr>
     <td width="60%" class="statlink"><h2>{ALBUM_NAME}</h2></td>
     <td width="40%" class="sortorder_cell">
		<ul class="menu" style="text-align: left">
			<li class="top"><a id="Sort" class="top_link"><b class="b_top"></b>
			<span class="span_top drop">{SORT_BY}</span></a>
			<ul class="sub">
				<li>
					<a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=ta" title="{SORT_TA}">
					<b class="UpArrow"></b><span>{SORT_TA}</span></a> </li>
				<li>
					<a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=td" title="{SORT_TD}">
					<b class="DownArrow"></b><span>{SORT_TA}</span> </a></li>
				<li>
					<a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=na" title="{SORT_NA}">
					<b class="UpArrow"></b><span>{SORT_NA}</span> </a></li>
				<li>
					<a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=nd" title="{SORT_ND}">
					<b class="DownArrow"></b><span>{SORT_ND}</span> </a></li>
				<li>
					<a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=da" title="{SORT_DA}">
					<b class="UpArrow"></b><span>{SORT_DA}</span> </a></li>
				<li>
					<a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=dd" title="{SORT_DD}">
					<b class="DownArrow"></b><span>{SORT_DD}</span> </a></li>
				<li>
					<a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=pa" title="{SORT_PA}">
					<b class="UpArrow"></b><span>{SORT_PA}</span> </a></li>
				<li>
					<a href="thumbnails.php?album={AID}&amp;page={PAGE}&amp;sort=pd" title="{SORT_PD}">
					<b class="DownArrow"></b><span>{SORT_PD}</span> </a></li>
			</ul>
			</li>
		</ul>
    </td>
    </tr>
  </table>

EOT;

/******************************************************************************
** Section <<<$template_thumb_view_title_row>>> - END  
******************************************************************************/
/******************************************************************************
** Section <<<$template_album_admin_menu>>> - START  
******************************************************************************/

// HTML template for the ALBUM admin menu displayed in the album list
$template_album_admin_menu = <<<EOT
	<ul class="menu2">
		<li class="top"><a id="AlbumMgr" class="top_link"><b class="b_top"></b><span class="span_top drop">{ALBUM_CONFIG}</span></a>
			<ul class="sub">
				<li>
                 	<a href="delete.php?id={ALBUM_ID}&amp;what=album&amp;form_token={FORM_TOKEN}&amp;timestamp={TIMESTAMP}" onclick="return confirm('{CONFIRM_DELETE}');">
					<b class="Delete"></b><span>{DELETE}</span></a>
				</li>
	            <li>
	               	<a href="modifyalb.php?album={ALBUM_ID}" ><b class="AlbumProp"></b><span>{MODIFY}</span></a>
	            </li>
	            <li>
					<a href="editpics.php?album={ALBUM_ID}"><b class="EditMgr"></b><span>{EDIT_PICS}</span>
					</a>
				</li>
			</ul>
		</li>
	</ul>
EOT;
/******************************************************************************
** Section <<<$template_album_admin_menu>>> - END
******************************************************************************/
/******************************************************************************
** Section <<<$template_album_list>>> - START
******************************************************************************/
// HTML template for the album list
$template_album_list = <<<EOT

<!-- BEGIN stat_row -->
        <tr><td colspan="{COLUMNS}" class="tableh1" align="center"><span class="statlink">{STATISTICS}</span></td></tr>
<!-- END stat_row -->
<!-- BEGIN header -->
        <tr class="tableb tableb_alternate">
<!-- END header -->
<!-- BEGIN album_cell -->
        <td width="{COL_WIDTH}%" valign="top">
        <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
         <td colspan="3" height="1" align="left" valign="top" class="tableh2">
           <span class="alblink"><a href="{ALB_LINK_TGT}">{ALBUM_TITLE}</a></span>
         </td>
        </tr>
        <tr>
         <td align="center" valign="middle" class="thumbnails">
           <img src="images/spacer.gif" width="{THUMB_CELL_WIDTH}" height="1" class="image" style="margin-top: 0px; margin-bottom: 0px; border: none;" alt="spacer" /><br />
           <a href="{ALB_LINK_TGT}" class="albums">{ALB_LINK_PIC}<br /></a>
         </td>
         <td width="100%" valign="middle" align="left" class="tableb tableb_alternate">
           <table>
            <tr><td><b>{ALB_DESC}</b></td></tr>
           	<tr><td><p class="album_stat">{ALB_INFOS}</p></td></tr>
           	<tr><td width="200px">{ADMIN_MENU}</td></tr>
          </table>
         </td>
        </tr>
        </table>
        </td>
<!-- END album_cell -->
<!-- BEGIN empty_cell -->
        <td width="{COL_WIDTH}%" valign="top">
        <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
         <td height="1" valign="top" class="tableh2">&nbsp;</td>
        </tr>
        <tr>
         <td width="100%" valign="top" class="tableb tableb_alternate">
           <div class="thumbnails" style="background-color:transparent">
           <img src="images/spacer.gif" width="1" height="1" border="0" class="image" style="border:0;margin-top:1px;margin-bottom:0" alt="spacer" />
           </div>
         </td>
        </tr>
        </table>
        </td>
<!-- END empty_cell -->
<!-- BEGIN row_separator -->
        </tr>
        <tr class="tableb tableb_alternate">
<!-- END row_separator -->
<!-- BEGIN footer -->
        </tr>
<!-- END footer -->
<!-- BEGIN tabs -->
        <tr>
         <td colspan="{COLUMNS}" style="padding: 0px;">
           <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              {TABS}
            </tr>
           </table>
         </td>
        </tr>
<!-- END tabs -->
<!-- BEGIN spacer -->
<!--        <img src="images/spacer.gif" width="1" height="7" border="" alt="spacer" /><br /> -->
<!-- END spacer -->

EOT;
/******************************************************************************
** Section <<<$template_album_list>>> - END
******************************************************************************/
// HTML template for the album list
$template_album_list_cat = <<<EOT

<!-- BEGIN c_stat_row -->
        <tr>
                <td colspan="{COLUMNS}" class="tableh1" align="center"><span class="statlink">{STATISTICS}</span></td>
        </tr>
<!-- END c_stat_row -->
<!-- BEGIN c_header -->
        <tr class="tableb_compact">
<!-- END c_header -->
<!-- BEGIN c_album_cell -->
        <td width="{COL_WIDTH}%" valign="top">
        <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
                <td colspan="3" height="1" valign="top" class="tableh2">
                        <span class="alblink"><a href="{ALB_LINK_TGT}"><b>{ALBUM_TITLE}</b></a></span>
                </td>
        </tr>
        <tr>
                <td colspan="3">
                        <img src="images/spacer.gif" width="1" height="1" border="0" alt="spacer" /><br />
                </td>
        </tr>
        <tr>
                <td align="center" valign="middle" class="thumbnails">
                        <img src="images/spacer.gif" width="{THUMB_CELL_WIDTH}" height="1" class="image" style="margin-top: 0px; margin-bottom: 0px; border: none;" alt="spacer" /><br />
                        <a href="{ALB_LINK_TGT}" class="albums">{ALB_LINK_PIC}<br /></a>
                </td>
                <td>
                        <img src="images/spacer.gif" width="1" height="1" border="0" alt="spacer" />
                </td>
                <td width="100%" valign="top" class="tableb_compact">
                        {ADMIN_MENU}
                        <p>{ALB_DESC}</p>
                        <p class="album_stat">{ALB_INFOS}</p>
                </td>
        </tr>
        </table>
        </td>
<!-- END c_album_cell -->
<!-- BEGIN c_empty_cell -->
        <td width="{COL_WIDTH}%" valign="top">
        <table width="100%" cellspacing="0" cellpadding="0" >
        <tr>
                <td height="1" valign="top" class="tableh2">
                        <b>&nbsp;</b>
                </td>
        </tr>
        <tr>
                <td>
                        <img src="images/spacer.gif" width="1" height="1" border="0" alt="spacer" /><br />
                </td>
        </tr>
        <tr>
                <td width="100%" valign="top" class="tableb_compact" >
                      <div class="thumbnails" style="background-color:transparent"><img src="images/spacer.gif" width="1" height="1" border="0" class="image" style="border:0;margin-top:1px;margin-bottom:0" alt="spacer" /></div>
                </td>
        </tr>
        </table>
        </td>
<!-- END c_empty_cell -->
<!-- BEGIN c_row_separator -->
        </tr>
        <tr class="tableb_compact">
<!-- END c_row_separator -->
<!-- BEGIN c_footer -->
        </tr>
<!-- END c_footer -->
<!-- BEGIN c_tabs -->
        <tr>
                <td colspan="{COLUMNS}" style="padding: 0px;">
                        <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                       {TABS}
                                </tr>
                        </table>
                </td>
        </tr>
<!-- END c_tabs -->
<!-- BEGIN spacer -->
<!-- END spacer -->

EOT;
// HTML template for thumbnails display
$template_thumbnail_view = <<<EOT

<!-- BEGIN header -->
        <tr>
<!-- END header -->
<!-- BEGIN thumb_cell -->
        <td valign="top" class="thumbnails" width ="{CELL_WIDTH}" align="center">
                <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                                <td align="center">
                                        <a href="{LINK_TGT}">{THUMB}<br /></a>
                                        {CAPTION}
                                        {ADMIN_MENU}
                                </td>
                        </tr>
                </table>
        </td>
<!-- END thumb_cell -->
<!-- BEGIN empty_cell -->
                <td valign="top" class="thumbnails" align="center">&nbsp;</td>
<!-- END empty_cell -->
<!-- BEGIN row_separator -->
        </tr>
        <tr>
<!-- END row_separator -->
<!-- BEGIN footer -->
        </tr>
<!-- END footer -->
<!-- BEGIN tabs -->
        <tr>
                <td colspan="{THUMB_COLS}" style="padding: 0px;">
                        <table width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                       {TABS}
                                </tr>
                        </table>
                </td>
        </tr>
<!-- END tabs -->
<!-- BEGIN spacer -->
<!-- END spacer -->

EOT;

// HTML template for the category list
$template_cat_list = <<<EOT
<!-- BEGIN header -->
        <tr>
                <td class="tableh1" width="80%" align="left"><b>{CATEGORY}</b></td>
                <td class="tableh1" width="10%" align="center"><b>{ALBUMS}</b></td>
                <td class="tableh1" width="10%" align="center"><b>{PICTURES}</b></td>
        </tr>
<!-- END header -->
<!-- BEGIN catrow_noalb -->
        <tr>
                <td class="catrow_noalb" colspan="3"><table border="0"><tr><td align="left">{CAT_THUMB}</td><td align="left"><span class="catlink"><b>{CAT_TITLE}</b></span>{CAT_DESC}</td></tr></table></td>
        </tr>
<!-- END catrow_noalb -->
<!-- BEGIN catrow -->
        <tr>
                <td class="catrow" align="left"><table border="0"><tr><td>{CAT_THUMB}</td><td><span class="catlink"><b>{CAT_TITLE}</b></span>{CAT_DESC}</td></tr></table></td>
                <td class="catrow" align="center">{ALB_COUNT}</td>
                <td class="catrow" align="center">{PIC_COUNT}</td>
        </tr>
        <tr>
            <td class="tableb" colspan="3">{CAT_ALBUMS}</td>
        </tr>
<!-- END catrow -->
<!-- BEGIN footer -->
        <tr>
                <td colspan="3" class="tableh1" align="center"><span class="statlink">{STATISTICS}</span></td>
        </tr>
<!-- END footer -->
<!-- BEGIN spacer -->
<!-- END spacer -->

EOT;

/******************************************************************************
** Section <<<$template_image_rating>>> - START
******************************************************************************/
// HTML template for the image rating box
$template_image_rating = <<<EOT
<table >
       <tr><td><b>{TITLE}</b> {VOTES}</td></tr>    
       <tr><td class="tableb_compact" width="65px" align="center"><a href="{RATE0}" title="{RUBBISH}" rel="nofollow"><img src="{LOCATION}images/rating0.gif" border="0" alt="{RUBBISH}" /><br /></a></td></tr>
       <tr><td class="tableb_compact" width="65px" align="center"><a href="{RATE1}" title="{POOR}" rel="nofollow"><img src="{LOCATION}images/rating1.gif" border="0" alt="{POOR}" /><br /></a></td></tr>
       <tr><td class="tableb_compact" width="65px" align="center"><a href="{RATE2}" title="{FAIR}" rel="nofollow"><img src="{LOCATION}images/rating2.gif" border="0" alt="{FAIR}" /><br /></a></td></tr>
       <tr><td class="tableb_compact" width="65px" align="center"><a href="{RATE3}" title="{GOOD}" rel="nofollow"><img src="{LOCATION}images/rating3.gif" border="0" alt="{GOOD}" /><br /></a></td></tr>
       <tr><td class="tableb_compact" width="65px" align="center"><a href="{RATE4}" title="{EXCELLENT}" rel="nofollow"><img src="{LOCATION}images/rating4.gif" border="0" alt="{EXCELLENT}" /><br /></a></td></tr>
       <tr><td class="tableb_compact" width="65px" align="center"><a href="{RATE5}" title="{GREAT}" rel="nofollow"><img src="{LOCATION}images/rating5.gif" border="0" alt="{GREAT}" /><br /></a></td></tr>
	  	
</table>
EOT;
/******************************************************************************
** Section <<<$template_image_rating>>> - END
******************************************************************************/
// HTML template for intermediate image display - HS: valign   <<<<<height="{CELL_HEIGHT}%"
$template_display_media = <<<EOT

   <td align="center" valign="top" class="display_media" >
	    <table cellspacing="2" cellpadding="0" class="imageborder">
          <tr><td align="center">{IMAGE}</td></tr>
          <tr align="center"><td>{ADMIN_MENU}</td></tr>
	    </table>
		<!-- BEGIN img_desc -->
	    <table cellpadding="0" cellspacing="0" class="tableb">
			<!-- BEGIN title -->
	          <tr><td class="tableb"><center><b>{TITLE}</b></center></td></tr>
			<!-- END title -->
			<!-- BEGIN caption -->
	          <tr><td class="tableb"><center>{CAPTION}</center></td></tr>
			<!-- END caption -->
	    </table>
		<!-- END img_desc -->
   </td>

EOT;


// Function to start a 'standard' table
function starttable($width = '-1', $title = '', $title_colspan = '1', $zebra_class = '')
{
    global $CONFIG;

    if ($width == '-1') $width = $CONFIG['picture_table_width'];
    if ($width == '100%') $width = $CONFIG['main_table_width'];

    if ($title) {
    echo <<<EOT
		<!-- Start standard table title -->
		<table align="center" width="$width" cellspacing="1" cellpadding="0" class="maintable $zebra_class">
	        <tr>
               <td class="tableh1" colspan="$title_colspan">$title</td>
	        </tr>
		</table>
		<!-- Start standard table -->
		<table align="center" width="$width" cellspacing="1" cellpadding="0" class="maintableb">
EOT;
    } else {
        echo <<<EOT
			<!-- Start standard table notitle -->
			<table align="center" width="$width" cellspacing="1" cellpadding="0" class="maintable $zebra_class">
EOT;
    }
}

$template_img_navbar = <<<EOT
<tr><td>
 <div id="multi-level_nav_left">
  <ul class="menu">
	<!-- First button without submenus -->	
	<li class="top">
		<a href="{PREV_TGT}" title="{PREV_TITLE}" id="Prev" class="top_link"><b class="b_top"></b><span class="span_top"></span></a>
	</li>
	<li class="top">
		<a href="{NEXT_TGT}" title="{NEXT_TITLE}" id="Next" class="top_link"><b class="b_top"></b><span class="span_top"></span></a>
	</li>
	<li class="top">
		<a href="{THUMB_TGT}" title="{THUMB_TITLE}" id="Thumbnails" class="top_link"><b class="b_top"></b><span class="span_top"></span></a>
	</li>
	<!-- BEGIN report_file_button -->
	<li class="top">
    	<a href="{REPORT_TGT}" title="{REPORT_TITLE}" id="Report" class="top_link"><b class="b_top"></b><span class="span_top"></span></a>
	</li>
	<!-- END report_file_button -->
	<!-- BEGIN ecard_button -->
	<li class="top">
		<a href="{ECARD_TGT}"  title="{ECARD_TITLE}"  id="Ecard" class="top_link"><b class="b_top"></b><span class="span_top "></span></a>       
	</li>
	<!-- END ecard_button -->
    <!-- BEGIN pic_info_button -->
	<li class="top">
		<a href="javascript:;" title="{PIC_INFO_TITLE}" id="PicInfo" class="top_link" onclick="blocking('picinfo','yes','block'); return false;" ><b class="b_top"></b><span class="span_top "></span></a>
	</li>
	<!-- END pic_info_button -->
	<!-- BEGIN slideshow_button -->
	<li class="top">
			<a href="{SLIDESHOW_TGT}" title="{SLIDESHOW_TITLE}" id="Slide" class="top_link"><b class="b_top"></b><span class="span_top "></span></a>
	</li>
	<!-- END slideshow_button -->
  </ul>
  <div class="tableh1">
		{PIC_POS}       
  </div>
</div>
</td></tr>
EOT;
/* 
*/
/******************************************************************************
** Section <<<$template_add_your_comment>>> - START
******************************************************************************/
$template_add_your_comment = <<<EOT
<form id="post" action="db_input.php" method="post" name="post">
	<table align="center" cellpadding="0" cellspacing="1" class="maintable" width="{WIDTH}">
		<tr>
			<td class="tableh2" width="100%">{ADD_YOUR_COMMENT}</td>
		</tr>
		<tr>
			<td colspan="1">
			<table cellpadding="0" cellspacing="0" width="100%">
				<!-- BEGIN user_name_input -->
				<tr>
					<td class="tableb tableb_alternate">{NAME} </td>
					<td class="tableb tableb_alternate">
					<input class="textinput" maxlength="20" name="msg_author" size="10" type="text" value="{USER_NAME}" />
					</td>
					<!-- END user_name_input -->
					<!-- BEGIN input_box_smilies -->
					<td class="tableb tableb_alternate">{COMMENT} </td>
					<td class="tableb tableb_alternate" width="100%">
					<input id="message" class="textinput" maxlength="{MAX_COM_LENGTH}" name="msg_body" onclick="storeCaret_post(this);" onkeyup="storeCaret_post(this);" onselect="storeCaret_post(this);" style="width: 100%;" type="text" />
					</td>
					<!-- END input_box_smilies -->
					<!-- BEGIN input_box_no_smilies -->
					<td class="tableb tableb_alternate">{COMMENT} </td>
					<td class="tableb tableb_alternate" width="100%">
					<input id="message" class="textinput" maxlength="{MAX_COM_LENGTH}" name="msg_body" style="width: 100%;" type="text" />
					</td>
					<!-- END input_box_no_smilies -->
					<!-- BEGIN submit -->
					<td class="tableb tableb_alternate">
					<input name="event" type="hidden" value="comment" />
					<input name="pid" type="hidden" value="{PIC_ID}" />
					<input class="buttonOK" name="submit" onclick="return notDefaultUsername(this.form, '{DEFAULT_USERNAME}', '{DEFAULT_USERNAME_MESSAGE}');" type="submit" value="{OK}" />
					<input name="form_token" type="hidden" value="{FORM_TOKEN}" />
					<input name="timestamp" type="hidden" value="{TIMESTAMP}" />
					</td>
					<!-- END submit -->
				</tr>
			</table>
			</td>
		</tr>
		<!-- BEGIN smilies -->
		<tr>
			<td class="tableb tableb_alternate" width="100%">{SMILIES} </td>
		</tr>
		<!-- END smilies -->
	</table>
</form>
EOT;
/******************************************************************************
** Section <<<$template_add_your_comment>>> - END
******************************************************************************/


// HTML template for filmstrip display
$template_film_strip = <<<EOT
	<tr><td>
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
			 <td valign="top" style="background-image: url({TILE1});"><img src="{TILE1}" alt="{TILE1}" border="0" /></td>
	    	 <!--begin thumb -->
			 <td>
			   <table style="width: 100%">
	    		<tr>
			     <td> {THUMB_STRIP} </td>
			    </tr>
			   </table>
			 </td>	
			 <!--end thumb -->
			 <td valign="top" style="background-image: url({TILE2});"><img src="{TILE2}" alt="{TILE2}" border="0" /></td>
			</tr>
		</table>
	</td></tr>

    <!-- BEGIN thumb_cell -->
	<table width="100%" cellspacing="0" cellpadding="0">
	  <tr>
	    <td align="center" valign="bottom"><a href="{LINK_TGT}">{THUMB}</a></td>
	  </tr>
	</table>
	<!-- END thumb_cell -->
	<!-- BEGIN empty_cell -->
	<td valign="top" align="center">&nbsp;</td>
	<!-- END empty_cell -->
EOT;

function theme_html_picinfo(&$info)
{
    global $lang_picinfo;
	$i = 0;
	
    $html = '';

    $html .= "        <tr><td colspan=\"2\" class=\"tableh2_compact\"><b>{$lang_picinfo['title']}</b></td></tr>\n";
    $template 		= "        <tr><td class=\"tableb_compact\" valign=\"top\" >%s:</td><td class=\"tableb_compact\">%s</td></tr>\n";
    $template_alt 	= "        <tr><td class=\"tableb_compact_alt\" valign=\"top\" >%s:</td><td class=\"tableb_compact_alt\">%s</td></tr>\n";
    foreach ($info as $key => $value) {
		$i += 1;    
		if ( $i&1 ){
    		$html .= sprintf($template, $key, $value); //$i is odd
		} else {
    		$html .= sprintf($template_alt, $key, $value); //$i is even
		}
    }
    return $html;
}


function theme_display_image($nav_menu, $picture, $votes, $pic_info, $comments, $film_strip)
{
	global $HTTP_COOKIE_VARS, $CONFIG;

	starttable();
	echo $nav_menu;
	echo "\n";
	endtable();
	starttable();
	if ($CONFIG['display_film_strip'] == 1) {
		echo "<tr><td width='150' class='tableb' valign='middle'>\n";
		echo "<!-- gb before film_strip -->\n";
		echo $film_strip;
		echo "</td>\n";		
		echo "<!-- gb after film_strip -->\n";
	}
	echo "<!-- gb before picture -->\n";
	echo $picture;
	echo "<td style=\"width: 80px\" class=\"tableb \" valign=\"top\" >";
	echo $votes;
	echo "</td></tr>\n<!-- gb after picture -->\n";
	endtable();

	$picinfo = isset($HTTP_COOKIE_VARS['picinfo']) ? $HTTP_COOKIE_VARS['picinfo'] : ($CONFIG['display_pic_info'] ? 'block' : 'none');
	echo "<div id=\"picinfo\" style=\"display: $picinfo;\">\n";
    starttable(); 
/*	echo '<table width="100%">'; */
	echo $pic_info;
/*	echo '</table>';
	echo "\n"; */
	endtable();
	echo "</div>\n";

    if($comments != "") {
    	starttable();
		echo $comments;
		echo "\n";
		endtable();
	}
}
/******************************************************************************
** Section <<<theme_admin_mode_menu>>> - START
******************************************************************************/
function theme_admin_mode_menu()
{
    global $cat;
    global $lang_gallery_admin_menu, $lang_user_admin_menu;
    global $template_gallery_admin_menu, $template_user_admin_menu;
    global $CONFIG;

    $cat_l = isset($cat) ? "?cat=$cat" : '';

    static $admin_menu = '';

    // Populate the admin menu only if empty to avoid template errors
    if ($admin_menu == '') {

        if (GALLERY_ADMIN_MODE) {

        if ($CONFIG['log_ecards'] == 0) {
            template_extract_block($template_gallery_admin_menu, 'log_ecards');
        }

        if (cpg_get_pending_approvals() == 0) {
             template_extract_block($template_gallery_admin_menu, 'admin_approval');
        }

            // do the docs exist on the webserver?
            if (file_exists('docs/index.htm') == true) {
                $documentation_href = 'docs/index.htm';
            } else {
                $documentation_href = 'http://coppermine-gallery.net/demo/cpg14x/docs/';
            }
            $param = array('{CATL}' => $cat_l,
                '{UPL_APP_TITLE}' => $lang_gallery_admin_menu['upl_app_title'],
                '{UPL_APP_LNK}' => $lang_gallery_admin_menu['upl_app_lnk'],
                '{ADMIN_TITLE}' => $lang_gallery_admin_menu['admin_title'],
                '{ADMIN_LNK}' => $lang_gallery_admin_menu['admin_lnk'],
                '{ALBUMS_TITLE}' => $lang_gallery_admin_menu['albums_title'],
                '{ALBUMS_LNK}' => $lang_gallery_admin_menu['albums_lnk'],
                '{CATEGORIES_TITLE}' => $lang_gallery_admin_menu['categories_title'],
                '{CATEGORIES_LNK}' => $lang_gallery_admin_menu['categories_lnk'],
                '{USERS_TITLE}' => $lang_gallery_admin_menu['users_title'],
                '{USERS_LNK}' => $lang_gallery_admin_menu['users_lnk'],
                '{GROUPS_TITLE}' => $lang_gallery_admin_menu['groups_title'],
                '{GROUPS_LNK}' => $lang_gallery_admin_menu['groups_lnk'],
                '{COMMENTS_TITLE}' => $lang_gallery_admin_menu['comments_title'],
                '{COMMENTS_LNK}' => $lang_gallery_admin_menu['comments_lnk'],
                '{SEARCHNEW_TITLE}' => $lang_gallery_admin_menu['searchnew_title'],
                '{SEARCHNEW_LNK}' => $lang_gallery_admin_menu['searchnew_lnk'],
                '{MY_PROF_TITLE}' => $lang_user_admin_menu['my_prof_title'],
                '{MY_PROF_LNK}' => $lang_user_admin_menu['my_prof_lnk'],
                '{UTIL_TITLE}' => $lang_gallery_admin_menu['util_title'],
                '{UTIL_LNK}' => $lang_gallery_admin_menu['util_lnk'],
                '{BAN_TITLE}' => $lang_gallery_admin_menu['ban_title'],
                '{BAN_LNK}' => $lang_gallery_admin_menu['ban_lnk'],
                '{DB_ECARD_TITLE}' => $lang_gallery_admin_menu['db_ecard_title'],
                '{DB_ECARD_LNK}' => $lang_gallery_admin_menu['db_ecard_lnk'],
                '{PICTURES_TITLE}' => $lang_gallery_admin_menu['pictures_title'],
                '{PICTURES_LNK}' => $lang_gallery_admin_menu['pictures_lnk'],
                '{DOCUMENTATION_HREF}' => $documentation_href,
                '{DOCUMENTATION_TITLE}' => $lang_gallery_admin_menu['documentation_title'],
                '{DOCUMENTATION_LNK}' => $lang_gallery_admin_menu['documentation_lnk'],
                '{PLUGINMGR_TITLE}' => $lang_gallery_admin_menu['pluginmgr_title'],
                '{PLUGINMGR_LNK}' => $lang_gallery_admin_menu['pluginmgr_lnk'],
                '{BRIDGEMGR_TITLE}' => $lang_gallery_admin_menu['bridgemgr_title'],
                '{BRIDGEMGR_LNK}' => $lang_gallery_admin_menu['bridgemgr_lnk'],
                '{PHPINFO_TITLE}' => $lang_gallery_admin_menu['phpinfo_title'],
                '{PHPINFO_LNK}' => $lang_gallery_admin_menu['phpinfo_lnk'],
                '{UPDATE_DATABASE_TITLE}' => $lang_gallery_admin_menu['update_database_title'],
                '{UPDATE_DATABASE_LNK}' => $lang_gallery_admin_menu['update_database_lnk'],
                '{VIEW_LOG_FILES_TITLE}' => $lang_gallery_admin_menu['view_log_files_title'],
                '{VIEW_LOG_FILES_LNK}' => $lang_gallery_admin_menu['view_log_files_lnk'],
                '{CHECK_VERSIONS_TITLE}' => $lang_gallery_admin_menu['check_versions_title'],
                '{CHECK_VERSIONS_LNK}' => $lang_gallery_admin_menu['check_versions_lnk'],
                '{OVERALL_STATS_TITLE}' => $lang_gallery_admin_menu['overall_stats_title'],
                '{OVERALL_STATS_LNK}' => $lang_gallery_admin_menu['overall_stats_lnk'],
                '{KEYWORDMGR_TITLE}' => $lang_gallery_admin_menu['keywordmgr_title'],
                '{KEYWORDMGR_LNK}' => $lang_gallery_admin_menu['keywordmgr_lnk'],
                '{EXIFMGR_TITLE}' => $lang_gallery_admin_menu['exifmgr_title'],
                '{EXIFMGR_LNK}' => $lang_gallery_admin_menu['exifmgr_lnk'],
                '{SHOWNEWS_TITLE}' => $lang_gallery_admin_menu['shownews_title'],
                '{SHOWNEWS_LNK}' => $lang_gallery_admin_menu['shownews_lnk'],
                '{EXPORT_TITLE}' => $lang_gallery_admin_menu['export_title'],
                '{EXPORT_LNK}' => $lang_gallery_admin_menu['export_lnk'],
		        '{ADM_CONFIG_LNK}' => $lang_gallery_admin_menu['adm_config_lnk'],		//gfy
		        '{ADM_CONFIG_TITLE}' => $lang_gallery_admin_menu['adm_config_title'],	//gfy
		        '{ADM_GALLERY_LNK}' => $lang_gallery_admin_menu['adm_gallery_lnk'],		//gfy
		        '{ADM_GALLERY_TITLE}' => $lang_gallery_admin_menu['adm_gallery_title'],	//gfy
		        '{ADM_GROUP_LNK}' => $lang_gallery_admin_menu['adm_group_lnk'],			//gfy
		        '{ADM_GROUP_TITLE}' => $lang_gallery_admin_menu['adm_group_title'],		//gfy
		        '{ADM_MEDIA_LNK}' => $lang_gallery_admin_menu['adm_media_lnk'],			//gfy
		        '{ADM_MEDIA_TITLE}' => $lang_gallery_admin_menu['adm_media_title'],		//gfy
                );

            $html = template_eval($template_gallery_admin_menu, $param);
            $html.= cpg_alert_dev_version();
        } elseif (USER_ADMIN_MODE) {
            $param = array('{ALBMGR_TITLE}' => $lang_user_admin_menu['albmgr_title'],
                '{ALBMGR_LNK}' => $lang_user_admin_menu['albmgr_lnk'],
                '{MODIFYALB_TITLE}' => $lang_user_admin_menu['modifyalb_title'],
                '{MODIFYALB_LNK}' => $lang_user_admin_menu['modifyalb_lnk'],
                '{MY_PROF_TITLE}' => $lang_user_admin_menu['my_prof_title'],
                '{MY_PROF_LNK}' => $lang_user_admin_menu['my_prof_lnk'],
                '{PICTURES_TITLE}' => $lang_gallery_admin_menu['pictures_title'],
                '{PICTURES_LNK}' => $lang_gallery_admin_menu['pictures_lnk'],
                '{USER_TOOLS_TITLE}' => $lang_user_admin_menu['user_tools_title'],	//gfy
                '{USER_TOOLS_LNK}' => $lang_user_admin_menu['user_tools_lnk'],	//gfy
                );

            $html = template_eval($template_user_admin_menu, $param);
        } else {
            $html = '';
        }

        $admin_menu = $html;
    }

    return $admin_menu;
}
/******************************************************************************
** Section <<<theme_admin_mode_menu>>> - END
******************************************************************************/
/******************************************************************************
** Section <<<$template_tab_display>>> - START
******************************************************************************/
$template_tab_display = array(
	'left_text' =>         '<!--[if IE ]> </tr><tr height="32px"> <![endif]--><td class="tabs_line" width="180px"><b>{LEFT_TEXT}</b></td>' . "\n",
    'allpages_dropdown' => '<td class="tabs_line" width="160px">%s  </td>' . "\n",
    'active_tab' =>   '<li class="top"><a               class="top_link_active"><b class="b_top"><span class="span_top_active">%d</span></b></a></li>' . "\n",
    'inactive_tab' =>       '<li class="top"><a href="{LINK}" class="top_link"><b class="b_top"><span class="span_top">%d</span></b></a></li>' . "\n",
    'nav_tab'      => '<li class="top"><a href="{LINK}" class="top_link"><b class="b_top"><span class="span_top">%d</span></b></a></li>' . "\n",
    'page_link'         => '{LINK}',
    'inactive_next_tab' => '<li class="top"><a href="{LINK}" class="top_link_right" title="{NEXT}"><b class="b_top"><span class="span_top_right"></span></b></a></li>' . "\n",
    'inactive_prev_tab' => '<li class="top"><a href="{LINK}" class="top_link_left"  title="{PREV}"><b class="b_top"><span class="span_top_left"></span></b></a></li>' . "\n",

);
/******************************************************************************
** Section <<<$template_tab_display>>> - END
******************************************************************************/

/******************************************************************************
** Section <<<theme_create_tabs>>> - START
******************************************************************************/
// Function for creating tabs showing multiple pages
function theme_create_tabs($items, $curr_page, $total_pages, $template)
{
    // Tabs do not take into account $lang_text_dir for RTL languages
    global $CONFIG, $lang_create_tabs;

    // Gallery Configuration setting for maximum number of tabs to display
    // Change this number if you want more/fewer tabs
    $maxTab = 10;

    if ($total_pages == '') {
        $total_pages = $curr_page;
    }

//    if (array_key_exists('page_link',$template)) {
        // Pass through links to tabs with links
//        $template['nav_tab']      = strtr($template['nav_tab'], array('{LINK}' => $template['page_link']));
//        $template['inactive_tab'] = strtr($template['inactive_tab'], array('{LINK}' => $template['page_link']));
//    }

    $tabs .= sprintf($template['left_text'], $items, $total_pages); // Left text, usually shows statistics
    if (($total_pages == 1)) {
        return $tabs;
    }

    if (1) {
        // Dropdown list for all pages
        $tabs_dropdown = $lang_create_tabs['jump'];
        $tabs_dropdown .= ' '. '<select onchange="if (this.options[this.selectedIndex].value != -1) { window.location.href = this.options[this.selectedIndex].value; }">';
        for ($page = 1; $page <= $total_pages; $page++) {
            $tabs_dropdown .= "\n" . '<option value="' . sprintf($template['page_link'], $page) . '"'
                    . ($page == $curr_page ? ' selected="selected"' : '') . '>' . $page .'</option>';
        }
        $tabs_dropdown .= '</select>' . "\n";
        $tabs .= sprintf($template['allpages_dropdown'], $tabs_dropdown);
    }

    // Calculate which pages to show on tabs, limited by the maximum number of tabs (set on Gallery Configuration panel)
    if ($total_pages > $maxTab) {
        $start = max(2, $curr_page - floor(($maxTab - 2) / 2));
        $start = min($start, $total_pages - $maxTab + 2);
        $end = $start + $maxTab - 3;
    } else {
        $start = 2;
        $end = $total_pages - 1;
    }
	$tabs .= '<td class="tabs_line"><ul class="tabs">';

    // Previous page tab
    if ($curr_page != ($start - 1)) {
    	$tabs .= strtr( sprintf($template['inactive_prev_tab'],$curr_page-1) , array('{PREV}' => $lang_create_tabs['previous']) );
    } else {
        // A previous tab with link is not needed.
        // If you want to show a disabled previous tab,
        //   create an image 'left_inactive.png', put it into themes/YOUR_THEME/images/icons/,
        //   then uncomment the line below.
        // $tabs .= sprintf($template['nav_tab_nolink'], cpg_fetch_icon('left_inactive',0,$lang_create_tabs['previous']));
    }

    // Page 1 tab
    if ($curr_page == 1) {
        $tabs .= sprintf($template['active_tab'], 1);
    } else {
        $tabs .= sprintf($template['inactive_tab'], 1, 1);
    }

    // Middle block of tabs
    for ($page = $start ; $page <= $end; $page++) {
        if ($page == $curr_page) {
            $tabs .= sprintf($template['active_tab'], $page);
        } else {
            $tabs .= sprintf($template['inactive_tab'], $page, $page);
        }
    }

    if ($total_pages > 1) {
        if ($curr_page == $total_pages) {
            $tabs .= sprintf($template['active_tab'], $total_pages);
        } else {
            $tabs .= sprintf($template['inactive_tab'], $total_pages, $total_pages);
        }
    }

    // Next page tab
    if ($curr_page != $total_pages) {
    	$tabs .= strtr( sprintf($template['inactive_next_tab'], $curr_page + 1) , array('{NEXT}' => $lang_create_tabs['next']) );
    } else {
        // A next tab with link is not needed.
        // If you want to show a disabled next tab,
        //   create an image 'right_inactive.png', put it into themes/YOUR_THEME/images/icons/,
        //   then uncomment the line below.
        // $tabs .= sprintf($template['nav_tab_nolink'], cpg_fetch_icon('right_inactive',0,$lang_create_tabs['next']));
    }

    // Trailer for tabs
    $tabs .= '</ul></td>';

    return $tabs;
}
/******************************************************************************
** Section <<<theme_create_tabs>>> - END
******************************************************************************/
function theme_display_album_list(&$alb_list, $nbAlb, $cat, $page, $total_pages)
{

    global $CONFIG, $STATS_IN_ALB_LIST, $statistics, $template_tab_display, $template_album_list, $lang_album_list;

    $theme_alb_list_tab_tmpl = $template_tab_display;

    $theme_alb_list_tab_tmpl['left_text'] = strtr($theme_alb_list_tab_tmpl['left_text'], array('{LEFT_TEXT}' => $lang_album_list['album_on_page']));
    $theme_alb_list_tab_tmpl['inactive_tab'] = strtr($theme_alb_list_tab_tmpl['inactive_tab'], array('{LINK}' => 'index.php?cat=' . $cat . '&amp;page=%d'));
    $theme_alb_list_tab_tmpl['inactive_next_tab'] = strtr($theme_alb_list_tab_tmpl['inactive_next_tab'], array('{LINK}' => 'index.php?cat=' . $cat . '&amp;page=%d'));
    $theme_alb_list_tab_tmpl['inactive_prev_tab'] = strtr($theme_alb_list_tab_tmpl['inactive_prev_tab'], array('{LINK}' => 'index.php?cat=' . $cat . '&amp;page=%d'));
//gfy added next line to pass the link for drop down page links 
    $theme_alb_list_tab_tmpl['page_link'] = strtr($theme_alb_list_tab_tmpl['page_link'], array('{LINK}' => 'index.php?cat=' . $cat . '&amp;page=%d'));

    $tabs = create_tabs($nbAlb, $page, $total_pages, $theme_alb_list_tab_tmpl);

    $album_cell = template_extract_block($template_album_list, 'album_cell');
    $empty_cell = template_extract_block($template_album_list, 'empty_cell');
    $tabs_row = template_extract_block($template_album_list, 'tabs');
    $stat_row = template_extract_block($template_album_list, 'stat_row');
    $spacer = template_extract_block($template_album_list, 'spacer');
    $header = template_extract_block($template_album_list, 'header');
    $footer = template_extract_block($template_album_list, 'footer');
    $rows_separator = template_extract_block($template_album_list, 'row_separator');

    $count = 0;

    $columns = $CONFIG['album_list_cols'];
    $column_width = ceil(100 / $columns);
    $thumb_cell_width = $CONFIG['alb_list_thumb_size'] + 2;

    starttable('100%');

    if ($STATS_IN_ALB_LIST) {
        $params = array('{STATISTICS}' => $statistics,
            '{COLUMNS}' => $columns,
            );
        echo template_eval($stat_row, $params);
    }

    echo $header;

    if (is_array($alb_list)) {
        foreach($alb_list as $album) {
            $count ++;

            $params = array('{COL_WIDTH}' => $column_width,
                '{ALBUM_TITLE}' => $album['album_title'],
                '{THUMB_CELL_WIDTH}' => $thumb_cell_width,
                '{ALB_LINK_TGT}' => "thumbnails.php?album={$album['aid']}",
                '{ALB_LINK_PIC}' => $album['thumb_pic'],
                '{ADMIN_MENU}' => $album['album_adm_menu'],
                '{ALB_DESC}' => $album['album_desc'],
                '{ALB_INFOS}' => $album['album_info'],
                );

            echo template_eval($album_cell, $params);

            if ($count % $columns == 0 && $count < count($alb_list)) {
                echo $rows_separator;
            }
        }
    }

    $params = array('{COL_WIDTH}' => $column_width,
          '{SPACER}' => $thumb_cell_width
          );
    $empty_cell = template_eval($empty_cell, $params);

    while ($count++ % $columns != 0) {
        echo $empty_cell;
    }

    echo $footer;
    // Tab display
    $params = array('{COLUMNS}' => $columns,
        '{TABS}' => $tabs,
        );
    echo template_eval($tabs_row, $params);

    endtable();

    echo $spacer;
}


function theme_display_album_list_cat(&$alb_list, $nbAlb, $cat, $page, $total_pages)
{
    global $CONFIG, $STATS_IN_ALB_LIST, $statistics, $template_tab_display, $template_album_list_cat, $lang_album_list;
    if (!$CONFIG['first_level']) {
        return;
    }

    $theme_alb_list_tab_tmpl = $template_tab_display;

    $theme_alb_list_tab_tmpl['left_text'] = strtr($theme_alb_list_tab_tmpl['left_text'], array('{LEFT_TEXT}' => $lang_album_list['album_on_page']));
    $theme_alb_list_tab_tmpl['inactive_tab'] = strtr($theme_alb_list_tab_tmpl['inactive_tab'], array('{LINK}' => 'index.php?cat=' . $cat . '&amp;page=%d'));
//gfy added next line to pass the link for drop down page links 
    $theme_alb_list_tab_tmpl['page_link'] = strtr($theme_alb_list_tab_tmpl['page_link'], array('{LINK}' => 'index.php?cat=' . $cat . '&amp;page=%d'));

    $tabs = create_tabs($nbAlb, $page, $total_pages, $theme_alb_list_tab_tmpl);
    // echo $template_album_list_cat;
    $template_album_list_cat1 = $template_album_list_cat;
    $album_cell = template_extract_block($template_album_list_cat1, 'c_album_cell');
    $empty_cell = template_extract_block($template_album_list_cat1, 'c_empty_cell');
    $tabs_row = template_extract_block($template_album_list_cat1, 'c_tabs');
    $stat_row = template_extract_block($template_album_list_cat1, 'c_stat_row');
    $spacer = template_extract_block($template_album_list_cat1, 'c_spacer');
    $header = template_extract_block($template_album_list_cat1, 'c_header');
    $footer = template_extract_block($template_album_list_cat1, 'c_footer');
    $rows_separator = template_extract_block($template_album_list_cat1, 'c_row_separator');

    $count = 0;

    $columns = $CONFIG['album_list_cols'];
    $column_width = ceil(100 / $columns);
    $thumb_cell_width = $CONFIG['alb_list_thumb_size'] + 2;

    starttable('100%');

    if ($STATS_IN_ALB_LIST) {
        $params = array('{STATISTICS}' => $statistics,
            '{COLUMNS}' => $columns,
            );
        echo template_eval($stat_row, $params);
    }

    echo $header;

    if (is_array($alb_list)) {
        foreach($alb_list as $album) {
            $count ++;

            $params = array('{COL_WIDTH}' => $column_width,
                '{ALBUM_TITLE}' => $album['album_title'],
                '{THUMB_CELL_WIDTH}' => $thumb_cell_width,
                '{ALB_LINK_TGT}' => "thumbnails.php?album={$album['aid']}",
                '{ALB_LINK_PIC}' => $album['thumb_pic'],
                '{ADMIN_MENU}' => $album['album_adm_menu'],
                '{ALB_DESC}' => $album['album_desc'],
                '{ALB_INFOS}' => $album['album_info'],
                );

            echo template_eval($album_cell, $params);

            if ($count % $columns == 0 && $count < count($alb_list)) {
                echo $rows_separator;
            }
        }
    }

    $params = array('{COL_WIDTH}' => $column_width,
          '{SPACER}' => $thumb_cell_width
          );
    $empty_cell = template_eval($empty_cell, $params);

    while ($count++ % $columns != 0) {
        echo $empty_cell;
    }

    echo $footer;
    // Tab display
    $params = array('{COLUMNS}' => $columns,
        '{TABS}' => $tabs,
        );
    echo template_eval($tabs_row, $params);

    endtable();

    echo $spacer;
}


function theme_display_thumbnails(&$thumb_list, $nbThumb, $album_name, $aid, $cat, $page, $total_pages, $sort_options, $display_tabs, $mode = 'thumb')
{
    global $CONFIG;
    global $template_thumb_view_title_row,$template_fav_thumb_view_title_row, $lang_thumb_view, $template_tab_display, $template_thumbnail_view, $lang_album_list;

    static $header = '';
    static $thumb_cell = '';
    static $empty_cell = '';
    static $row_separator = '';
    static $footer = '';
    static $tabs = '';
    static $spacer = '';

    if ($header == '') {
        $thumb_cell = template_extract_block($template_thumbnail_view, 'thumb_cell');
        $tabs = template_extract_block($template_thumbnail_view, 'tabs');
        $header = template_extract_block($template_thumbnail_view, 'header');
        $empty_cell = template_extract_block($template_thumbnail_view, 'empty_cell');
        $row_separator = template_extract_block($template_thumbnail_view, 'row_separator');
        $footer = template_extract_block($template_thumbnail_view, 'footer');
        $spacer = template_extract_block($template_thumbnail_view, 'spacer');
    }

    $cat_link = is_numeric($aid) ? '' : '&amp;cat=' . $cat;
    $uid_link = (isset($_GET['uid']) && is_numeric($_GET['uid'])) ? '&amp;uid=' . $_GET['uid'] : '';

    $theme_thumb_tab_tmpl = $template_tab_display;

    if ($mode == 'thumb') {
        $theme_thumb_tab_tmpl['left_text'] = strtr($theme_thumb_tab_tmpl['left_text'], array('{LEFT_TEXT}' => $aid == 'lastalb' ? $lang_album_list['album_on_page'] : $lang_thumb_view['pic_on_page']));
        $theme_thumb_tab_tmpl['inactive_tab'] = strtr($theme_thumb_tab_tmpl['inactive_tab'], array('{LINK}' => 'thumbnails.php?album=' . $aid . $cat_link . $uid_link . '&amp;page=%d'));
        $theme_thumb_tab_tmpl['inactive_next_tab'] = strtr($theme_thumb_tab_tmpl['inactive_next_tab'], array('{LINK}' => 'thumbnails.php?album=' . $aid . $cat_link . $uid_link . '&amp;page=%d'));
        $theme_thumb_tab_tmpl['inactive_prev_tab'] = strtr($theme_thumb_tab_tmpl['inactive_prev_tab'], array('{LINK}' => 'thumbnails.php?album=' . $aid . $cat_link . $uid_link . '&amp;page=%d'));
//gfy added next line to pass the link for drop down page links 
        $theme_thumb_tab_tmpl['page_link'] = strtr($theme_thumb_tab_tmpl['page_link'], array('{LINK}' => 'thumbnails.php?album=' . $aid . $cat_link . $uid_link . '&amp;page=%d'));
    } else {
        $theme_thumb_tab_tmpl['left_text'] = strtr($theme_thumb_tab_tmpl['left_text'], array('{LEFT_TEXT}' => $lang_thumb_view['user_on_page']));
        $theme_thumb_tab_tmpl['inactive_tab'] = strtr($theme_thumb_tab_tmpl['inactive_tab'], array('{LINK}' => 'index.php?cat=' . $cat . '&amp;page=%d'));
        $theme_thumb_tab_tmpl['inactive_next_tab'] = strtr($theme_thumb_tab_tmpl['inactive_next_tab'], array('{LINK}' => 'index.php?cat=' . $cat . '&amp;page=%d'));
        $theme_thumb_tab_tmpl['inactive_prev_tab'] = strtr($theme_thumb_tab_tmpl['inactive_prev_tab'], array('{LINK}' => 'index.php?cat=' . $cat . '&amp;page=%d'));
//gfy added next line to pass the link for drop down page links   
     	$theme_thumb_tab_tmpl['page_link'] = strtr($theme_thumb_tab_tmpl['page_link'], array('{LINK}' => 'index.php?cat=' . $cat . '&amp;page=%d'));
    }

    $thumbcols = $CONFIG['thumbcols'];
    $cell_width = ceil(100 / $CONFIG['thumbcols']) . '%';

    $tabs_html = $display_tabs ? create_tabs($nbThumb, $page, $total_pages, $theme_thumb_tab_tmpl) : '';
    // The sort order options are not available for meta albums
    if ($sort_options) {
        $param = array('{ALBUM_NAME}' => $album_name,
            '{AID}' => $aid,
            '{PAGE}' => $page,
            '{NAME}' => $lang_thumb_view['name'],
            '{TITLE}' => $lang_thumb_view['title'],
            '{DATE}' => $lang_thumb_view['date'],
            '{SORT_BY}' => $lang_thumb_view['sort_by'],		//gfy
            '{SORT_TA}' => $lang_thumb_view['sort_ta'],
            '{SORT_TD}' => $lang_thumb_view['sort_td'],
            '{SORT_NA}' => $lang_thumb_view['sort_na'],
            '{SORT_ND}' => $lang_thumb_view['sort_nd'],
            '{SORT_DA}' => $lang_thumb_view['sort_da'],
            '{SORT_DD}' => $lang_thumb_view['sort_dd'],
            '{POSITION}' => $lang_thumb_view['position'],
            '{SORT_PA}' => $lang_thumb_view['sort_pa'],
            '{SORT_PD}' => $lang_thumb_view['sort_pd'],
            );
        $title = template_eval($template_thumb_view_title_row, $param);
    } else if ($aid == 'favpics' && $CONFIG['enable_zipdownload'] == 1) { //Lots of stuff can be added here later
       $param = array('{ALBUM_NAME}' => $album_name,
                             '{DOWNLOAD_ZIP}'=>$lang_thumb_view['download_zip']
                               );
       $title = template_eval($template_fav_thumb_view_title_row, $param);
    }else{
        $title = $album_name;
    }


    if ($mode == 'thumb') {
        starttable('100%', $title, $thumbcols);
    } else {
        starttable('100%');
    }

    echo $header;

    $i = 0;
    foreach($thumb_list as $thumb) {
        $i++;
        if ($mode == 'thumb') {
            if ($aid == 'lastalb') {
                $params = array('{CELL_WIDTH}' => $cell_width,
                    '{LINK_TGT}' => "thumbnails.php?album={$thumb['aid']}",
                    '{THUMB}' => $thumb['image'],
                    '{CAPTION}' => $thumb['caption'],
                    '{ADMIN_MENU}' => $thumb['admin_menu']
                    );
            } else {
                $params = array('{CELL_WIDTH}' => $cell_width,
                    '{LINK_TGT}' => "displayimage.php?album=$aid$cat_link&amp;pos={$thumb['pos']}$uid_link",
                    '{THUMB}' => $thumb['image'],
                    '{CAPTION}' => $thumb['caption'],
                    '{ADMIN_MENU}' => $thumb['admin_menu']
                    );
            }
        } else {
            $params = array('{CELL_WIDTH}' => $cell_width,
                '{LINK_TGT}' => "index.php?cat={$thumb['cat']}",
                '{THUMB}' => $thumb['image'],
                '{CAPTION}' => $thumb['caption'],
                '{ADMIN_MENU}' => ''
                );
        }
        echo template_eval($thumb_cell, $params);

        if ((($i % $thumbcols) == 0) && ($i < count($thumb_list))) {
            echo $row_separator;
        }
    }
    for (;($i % $thumbcols); $i++) {
        echo $empty_cell;
    }
    echo $footer;

    if ($display_tabs) {
        $params = array('{THUMB_COLS}' => $thumbcols,
            '{TABS}' => $tabs_html
            );
        echo template_eval($tabs, $params);
    }

    endtable();
    echo $spacer;
}
//{THEMES}
function theme_main_menu($which)
{
    global $AUTHORIZED, $CONFIG, $album, $actual_cat, $cat, $REFERER;
    global $lang_main_menu, $template_sys_menu, $template_sub_menu;


    static $sys_menu = '', $sub_menu = '';
    if ($$which != '') {
        return $$which;
    }

    $album_l = isset($album) ? "?album=$album" : '';
    $cat_l = (isset($actual_cat))? "?cat=$actual_cat" : (isset($cat) ? "?cat=$cat" : '');
    $cat_l2 = isset($cat) ? "&amp;cat=$cat" : '';
    $my_gallery_id = FIRST_USER_CAT + USER_ID;



  if ($which == 'sys_menu' ) {
    if (USER_ID) {
        template_extract_block($template_sys_menu, 'login');
    } else {
        template_extract_block($template_sys_menu, 'logout');
        template_extract_block($template_sys_menu, 'my_profile');
    }

    if (!USER_IS_ADMIN) {
        template_extract_block($template_sys_menu, 'enter_admin_mode');
        template_extract_block($template_sys_menu, 'leave_admin_mode');
    } else {
        if (GALLERY_ADMIN_MODE) {
            template_extract_block($template_sys_menu, 'enter_admin_mode');
        } else {
            template_extract_block($template_sys_menu, 'leave_admin_mode');
        }
    }

    if (!USER_CAN_CREATE_ALBUMS) {
        template_extract_block($template_sys_menu, 'my_gallery');
    }

    if (USER_CAN_CREATE_ALBUMS) {
        template_extract_block($template_sys_menu, 'my_profile');
    }

    if (!USER_CAN_UPLOAD_PICTURES && !USER_CAN_CREATE_ALBUMS) {
        template_extract_block($template_sys_menu, 'upload_pic');
    }

    if (USER_ID || !$CONFIG['allow_user_registration']) {
        template_extract_block($template_sys_menu, 'register');
    }

    if (!USER_ID || !$CONFIG['allow_memberlist']) {
        template_extract_block($template_sys_menu, 'allow_memberlist');
    }

    if (!$CONFIG['display_faq']) {
        template_extract_block($template_sys_menu, 'faq');
    }

    $param = array(
        '{HOME_TGT}' => $CONFIG['home_target'],
        '{HOME_TITLE}' => $lang_main_menu['home_title'],
        '{HOME_LNK}' => $lang_main_menu['home_lnk'],
        '{MY_GAL_TGT}' => "index.php?cat=$my_gallery_id",
        '{MY_GAL_TITLE}' => $lang_main_menu['my_gal_title'],
        '{MY_GAL_LNK}' => $lang_main_menu['my_gal_lnk'],
        '{MEMBERLIST_TGT}' => "usermgr.php",
        '{MEMBERLIST_TITLE}' => $lang_main_menu['memberlist_title'],
        '{MEMBERLIST_LNK}' => $lang_main_menu['memberlist_lnk'],
        '{MY_PROF_TGT}' => "profile.php?op=edit_profile",
        '{MY_PROF_TITLE}' => $lang_main_menu['my_prof_title'],
        '{MY_PROF_LNK}' => $lang_main_menu['my_prof_lnk'],
        '{ADM_MODE_TGT}' => "mode.php?admin_mode=1&amp;referer=$REFERER",
        '{ADM_MODE_TITLE}' => $lang_main_menu['adm_mode_title'],
        '{ADM_MODE_LNK}' => $lang_main_menu['adm_mode_lnk'],
        '{USR_MODE_TGT}' => "mode.php?admin_mode=0&amp;referer=$REFERER",
        '{USR_MODE_TITLE}' => $lang_main_menu['usr_mode_title'],
        '{USR_MODE_LNK}' => $lang_main_menu['usr_mode_lnk'],
        '{UPL_PIC_TGT}' => "upload.php",
        '{UPL_PIC_TITLE}' => $lang_main_menu['upload_pic_title'],
        '{UPL_PIC_LNK}' => $lang_main_menu['upload_pic_lnk'],
        '{REGISTER_TGT}' => "register.php",
        '{REGISTER_TITLE}' => $lang_main_menu['register_title'],
        '{REGISTER_LNK}' => $lang_main_menu['register_lnk'],
        '{LOGIN_TGT}' => "login.php?referer=$REFERER",
        '{LOGIN_TITLE}' => $lang_main_menu['login_title'],
        '{LOGIN_LNK}' => $lang_main_menu['login_lnk'],
        '{LOGOUT_TGT}' => "logout.php?referer=$REFERER",
        '{LOGOUT_TITLE}' => $lang_main_menu['logout_title'],
        '{LOGOUT_LNK}' => $lang_main_menu['logout_lnk'] . " [" . stripslashes(USER_NAME) . "]",
        '{FAQ_TGT}' => "faq.php",
        '{FAQ_TITLE}' => $lang_main_menu['faq_title'],
        '{FAQ_LNK}' => $lang_main_menu['faq_lnk'],
        '{SYS_USER_LNK}' => $lang_main_menu['sys_user_lnk'],		//gfy
        '{SYS_USER_TITLE}' => $lang_main_menu['sys_user_title'],	//gfy
        );

        $sys_menu = template_eval($template_sys_menu, $param);
  } else {

    if (!$CONFIG['custom_lnk_url']) {
        template_extract_block($template_sub_menu, 'custom_link');
    }

    $param = array(
        '{ALB_LIST_TGT}' => "index.php$cat_l",
        '{ALB_LIST_TITLE}' => $lang_main_menu['alb_list_title'],
        '{ALB_LIST_LNK}' => $lang_main_menu['alb_list_lnk'],
        '{CUSTOM_LNK_TGT}' => $CONFIG['custom_lnk_url'],
        '{CUSTOM_LNK_TITLE}' => $CONFIG['custom_lnk_name'],
        '{CUSTOM_LNK_LNK}' => $CONFIG['custom_lnk_name'],
        '{LASTUP_TGT}' => "thumbnails.php?album=lastup$cat_l2",
        '{LASTUP_TITLE}' => $lang_main_menu['lastup_title'],
        '{LASTUP_LNK}' => $lang_main_menu['lastup_lnk'],
        '{LASTCOM_TGT}' => "thumbnails.php?album=lastcom$cat_l2",
        '{LASTCOM_TITLE}' => $lang_main_menu['lastcom_title'],
        '{LASTCOM_LNK}' => $lang_main_menu['lastcom_lnk'],
        '{TOPN_TGT}' => "thumbnails.php?album=topn$cat_l2",
        '{TOPN_TITLE}' => $lang_main_menu['topn_title'],
        '{TOPN_LNK}' => $lang_main_menu['topn_lnk'],
        '{TOPRATED_TGT}' => "thumbnails.php?album=toprated$cat_l2",
        '{TOPRATED_TITLE}' => $lang_main_menu['toprated_title'],
        '{TOPRATED_LNK}' => $lang_main_menu['toprated_lnk'],
        '{FAV_TGT}' => "thumbnails.php?album=favpics",
        '{FAV_TITLE}' => $lang_main_menu['fav_title'],
        '{FAV_LNK}' => $lang_main_menu['fav_lnk'],
        '{SEARCH_TGT}' => "search.php",
        '{SEARCH_TITLE}' => $lang_main_menu['search_title'],
        '{SEARCH_LNK}' => $lang_main_menu['search_lnk'],
        '{SUB_ALBUM_LNK}' => $lang_main_menu['sub_album_lnk'],		//gfy
        '{SUB_ALBUM_TITLE}' => $lang_main_menu['sub_album_title'],	//gfy
        );
    $sub_menu = template_eval($template_sub_menu, $param);
  }

    return $$which;
}

?>
