/*  
Theme Name: Grey-style-20



Ported to CPG  by: Frantz
Porter URI: http://f.keller.free.fr
Coppermine Theme Version: 1.0.2
Tested on CPG v1.4.19
*/

License -
The CSS, XHTML and design is released under GPL:
http://www.opensource.org/licenses/gpl-license.php

This program is free software; you can redistribute it and/or modify it under 

The terms of the GNU General Public License as published by the Free Software 
Foundation, version 2 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 

PARTICULAR PURPOSE. See the GNU General Public License for more details.
*/

Thank you for downloading Grey-style-20, a  theme for Coppermine Photo Gallery v1.4.x ported and expanded from a WordPress theme. This is a fixed width theme of 860 pixels and is fully compatible with most of the major browsers (I tested it in FF, IE ). 

I've overwritten some of the configuration settings by using variables in the theme.php file and added a new variable for retrieving the album description. The reason for this is to prevent the standard settings in the Configuration tables set by you from "breaking" the theme. These changes are:

// These parameters overide what the user inputs in the Configuration setup to prevent the theme from breaking.
$CONFIG['max_film_strip_items'] = 4; //overrides the number of thumbnails.
$CONFIG['thumbcols'] = 4; //overrides the number of columns for thumbnails.
$CONFIG['main_table_width'] = '100%'; //overrides the Width of the main table (pixels or %).
$CONFIG['picture_table_width'] = '100%'; //overrides the Width of the table for file display (pixels or %).

There is also a new function for truncating the album descriptions when viewing on the index or category pages. When you view a single album, the full description is printed out at the top of the album. This makes the index and category pages much cleaner looking. I've also adapted it to truncate long image captions when viewing on a thumbnail page but prints the full caption when viewing the intermediate image.

To edit the number of characters in the truncated album description, find in 2 places in the theme.php file - '{ALB_DESC}' => myTruncate($album['album_desc'], 20, " "), and change the number (20) to any length your desire. You can also change the padding characters (...) to any other set of characters by editing function myTruncate($string, $limit, $break=".", $pad="...") in the theme.php file.

To edit the number of characters in the truncated image caption, find in 2 places in the theme.php file - '{CAPTION}' => myTruncate($caption, 120, " "), // changing the number changes the # of characters printed for the thumbnail caption. and change the number (120) to an length you desire. This number doesn't seem to be match the string length but it is consistant so change it and view the results.

I've also adapted the compute_img_size($width, $height, $max) function to manage the size of the intermediate image. As in some cases (example would be the Coppermine demo page), some galleries have intermediate images of different sizes. This function was changed and added to the theme.php to make view all intermediate images at 400px wide. This function is now compute_img_size_max($width, $height) and you can change the view size by editing $max = 400;. If you have intermediate images smaller than 400px wide then this function will increase the image size when viewing which could result in a distorted image. Since the default size in Coppermine is "400", this should not be a problem unless you've decreased it. This setting in the theme.php file does not affect the setting in your Coppermine configuration but only resizes the images in the browser. Here's a link to additional info in the manual - http://coppermine-gallery.net/demo/cpg14x/docs/index.htm#admin_picture_thumbnail.
All this functions are from Gizmo. Many thank's to him.

Icons in the img folder come from http://www.vistaico.com
This theme comes with a multilingual menu title:
in the theme folder you find a lang folder with some language files (english, french, german).
feel free to add your own lang file an share it on the coppermine forum
Please enjoy the theme and all I ask is that you keep the credit line in the template.html to help support my efforts to develop and port themes for Coppermine.

Cheers,

Frantz